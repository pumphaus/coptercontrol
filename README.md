# CopterControl

This is a personal project to explore controlling a drone (a quadcopter, specifically) with only a Raspberry Pi 3 and Qt.

## Used components, software

* [DJI FlameWheel F450](https://www.dji.com/flame-wheel-arf/)
* LiPo battery pack
* [Raspberry Pi 3 Model A+](https://www.raspberrypi.com/products/raspberry-pi-3-model-a-plus/)
* GY-86 sensor module, containing:
  * MPU6050 accelerometer and gyroscope
  * HMC5883L compass
  * MS5611 barometric pressure sensor
* Modified parts of [I2CDevLib](https://github.com/jrowberg/i2cdevlib)
* [BlasterPP](https://github.com/pumphaus/blasterpp) for PWM output on any pin of the Raspberry Pi
* [Qt](https://www.qt.io)

A 3D printer and a soldering iron sure come in handy as well.

## Internals
 
### Timing
Sensor readout has to happen in a timely manner in order to get a steady sensor sampling rate and not lose any samples. I've experimented a lot with a stock Linux kernel and RT-patched kernels, but I simply couldn't get precisely timed callbacks in the controlling app to read out the sensors.

In the end, I've decided to use a brute force approach: the app spawns a thread that locks up a single core of the RPi 100% in a busy loop until there is some work to do (see `rtthread.{h,cpp}` and the timer in `Sensors` for that).

### Controlling the motors

The Raspberry Pi does not have enough PWM pins to control all four rotor motors, let alone additional hardware like servo motors. Fortunately, [some people](https://github.com/richardghirst/PiBits) [have figured out](https://github.com/sarfata/pi-blaster) how to utilize DMA and the PWM/PCM hardware to repurpose any pin as a PWM pin.

I've turned the approach into a reusable library [blasterpp](https://github.com/pumphaus/blasterpp) which is used in this project.

### Sensor fusion

To get a more accurate estimate of the various system parameters, sensor data measuring related parameters can be fused. I couldn't quite wrap my head around Kalman filtering yet, so instead I'm using what is commonly referred to as "complementary filtering".

A "complementary filter" is simply the sum of the low frequency parts of one sensor output and the high-frequency parts of another sensor output.

For example, to get a more accurate estimate of the roll and pitch angles, we can fuse the roll/pitch angles as calculated from the measured acceleration with the roll/pitch angles determined from integrating the gyro output. The gyro output will have a "random" DC offset, because it's unreferenced and will drift because of integration, but it reacts much more quickly to changes than the acceleration sensor. The acceleration sensor, on the other hand, will produce ground-referenced, non-drifting roll/pitch angles, but is noisy and doesn't react as quickly.  
To fuse these two, we sum up the low-pass filtered acceleration-based angles and the high-pass filtered gyro-based angles. The low-pass filter gets rid of the accelerometer noise and the high-pass filter gets rid of the offset and slow drifting of the gyro.

## Simulation

In order to play with all of this without actually having to start the actual quadcopter all the time, I've hacked [KDE Edu's Step](https://apps.kde.org/step/). It's a 2D physics simulator, which is fine for most of what I want to simulate.

So I've put my quadcopter on a weighing scale to measure its mass. I've then slowly increased the throttle on the rotors and tracked the scale's output to estimate how much lift the quadcopter generates for a given throttle setting.

The "model quadcopter" is then actually quite simple: A rectangular box with upwards-pointing "force generators" on its left and right edges.

The hack to get the `coptercontrol` flight controller into Step is really quite ugly: I'm building a slightly patched Step as a CMake subproject and then link in `coptercontrol` as a static library. Step has been modified so as to call a startup hook defined the `coptercontrol` static lib. It's quite nasty and does not support Step's "undo" functionality (yet), but other than that it works.
