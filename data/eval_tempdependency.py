#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 24 16:28:36 2019

@author: pumphaus
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as signal
import json
from itertools import chain

data = {}

def isiterable(x):
    try:
        iter(x)
        return True
    except TypeError:
        return False

with open('temp_dependency.txt', 'r') as f:
    for line in f:
        field, sep, entries = line.strip().partition(',')
        d = json.loads('[' + entries + ']')

        if field not in data:
            data[field] = []
        data[field].extend(d)

temp = np.reshape(data['imuTemperature'], (-1, 2))
accel = np.reshape(list(chain.from_iterable(x if isiterable(x) else [x] for x in data["uncalibratedAcceleration"])), (-1, 4))
gyro = np.reshape(list(chain.from_iterable(x if isiterable(x) else [x] for x in data["uncalibratedAngularVelocity"])), (-1, 4))

plt.figure()
plt.plot(temp[:, 0], temp[:, 1])
#plt.figure()
#plt.scatter(temp[:, 1], gyro[:, 1])

print(np.polyfit(temp[:, 1], gyro[:, 1], 1))
print(np.polyfit(temp[:, 1], gyro[:, 2], 1))
print(np.polyfit(temp[:, 1], gyro[:, 3], 1))
