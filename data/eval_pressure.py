#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 13:07:20 2019

@author: pumphaus
"""

import numpy as np
import matplotlib.pyplot as plt

pressure = []
altitude = []

def medianfilter(xs, winsize=5):
    ys = np.zeros(xs.shape)
    for i in range(winsize, len(xs)):
        ys[i] = np.median(xs[i-winsize:i])
    return ys

def lowpass(xs, dt=0.02, tau=0.3):
    alpha = dt / (tau + dt)
    ys = np.zeros(xs.shape)
    ys[0] = xs[0]
    for i in range(1, len(xs)):
        ys[i] = (1- alpha) * ys[i-1] + (alpha) * xs[i]
    return ys

with open('altimeter_vs_pressure.csv', 'r') as f:
    for line in f:
        if line.startswith('altitude'):
            for val in np.fromstring(line[9:], sep=','):
                altitude.append(val)
        elif line.startswith('pressure'):
            for val in np.fromstring(line[9:], sep=','):
                pressure.append(val)


def p2h(p):
    p0 = 101325
    rho0 = 1.2041
    g = 9.81
    return p0 / (rho0 * g) * np.log(p0 / p)


altitude = np.reshape(altitude, (-1, 2))
pressure = np.reshape(pressure, (-1, 2))

pressure_dt = np.mean(np.diff(pressure[:, 0])) / 1000

#plt.plot(pressure[:, 0], lowpass(p2h(pressure[:, 1]), dt=pressure_dt, tau=0.15))
plt.plot(pressure[:, 0], lowpass(np.gradient(p2h(pressure[:, 1])), dt=pressure_dt, tau=0.3))
plt.plot(altitude[:, 0], medianfilter(np.gradient(altitude[:, 1]), 10))
