#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 13:07:20 2019

@author: pumphaus
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as signal
import scipy.interpolate as interp

data = {}

def medianfilter(xs, winsize=5):
    ys = np.zeros(xs.shape)
    for i in range(winsize, len(xs)):
        ys[i] = np.median(xs[i-winsize:i])
    return ys

def lowpass(xs, dt=0.02, tau=0.3, butter=False):
    fc = 1/(2 * np.pi * tau)
#    print("Fc is", fc)
    b, a = signal.butter(2, fc, fs=1/dt)
    print(b, a)
#    print("b, a:", b, a)
    if butter:
        return signal.lfilter(b, a, xs)

    alpha = dt / (tau + dt)
    print("alpha:", alpha)
    ys = np.zeros(xs.shape)
    ys[0] = xs[0]
    for i in range(1, len(xs)):
        val = xs[i]
        if not np.isfinite(val):
            val = ys[i-1]
        ys[i] = (1- alpha) * ys[i-1] + (alpha) * val
    return ys

def complementary_fuse(xs, ds, dt, tau):
    alpha = dt / (tau + dt)
    ys = np.zeros(xs.shape)
    ys[0] = xs[0]
    for i in range(1, len(xs)):
        ys[i] = (1 - alpha) * (ys[i-1] + ds[i] * dt) + alpha * xs[i]
    return ys


def highpass(xs, dt=0.02, tau=0.3):
    alpha = dt / (tau + dt)
    ys = np.zeros(xs.shape)
    ys[0] = xs[0]
    for i in range(1, len(xs)):
        ys[i] = (1 - alpha) * (ys[i-1] + xs[i] - xs[i-1])
    return ys

def p2h(p):
    p0 = 101325
    rho0 = 1.2041
    g = 9.81
    return p0 / (rho0 * g) * np.log(p0 / p)


with open('vz_3sensor_new4.txt', 'r') as f:
    for line in f:
        entries = line.strip().split(',')
        field = entries[0]
        if field not in data:
            data[field] = []
        data[field].extend([ float(v) for v in entries[1:] ])

#        if line.startswith('altitude'):
#            for val in np.fromstring(line[9:], sep=','):
#                altitude.append(val)
#        elif line.startswith('pressure'):
#            for val in np.fromstring(line[9:], sep=','):
#                pressure.append(val)

for field in data:
    data[field] = np.reshape(data[field], (-1, 2))

az = data["az"]
relH = data["rawRelativeHeight"]
absH = data["rawAbsoluteHeight"]
cr = data["climbRate"]

t = az[:, 0] * 1e-3
dt = np.mean(np.diff(t))
fs = 1/dt

az = az[:, 1]
relH = interp.interp1d(relH[:, 0], relH[:, 1], fill_value='extrapolate')(t * 1e3)
absH = interp.interp1d(absH[:, 0], absH[:, 1], fill_value='extrapolate')(t * 1e3)
cr = interp.interp1d(cr[:, 0], cr[:, 1], fill_value='extrapolate')(t * 1e3)

#plt.semilogy(*signal.periodogram(relH, fs, 'hann'))

vz_from_a = np.cumsum((az - 1) * 9.81) * dt
vz_from_relH = np.gradient(relH, dt)
vz_from_absH = np.gradient(absH, dt)

fc = .5
tau = 1/(2 * np.pi * fc)
Wn = fc / (fs/2)
b, a = signal.butter(4, Wn)

plt.plot(t, vz_from_a)

vz_from_a_high = vz_from_a - signal.lfilter(b, a, vz_from_a)
vz_from_absH_low = signal.lfilter(b, a, vz_from_absH)

#vz_from_a_high = vz_from_a - signal.lfilter(b, a, signal.lfilter(b, a, vz_from_a))
#vz_from_absH_low = signal.lfilter(b, a, signal.lfilter(b, a, vz_from_absH))


#plt.plot(t, vz_from_a_high + vz_from_absH_low)
#vz_fused = complementary_fuse(vz_from_absH, (az - 1) * 9.81, dt, tau)
plt.plot(t, vz_from_a_high + vz_from_absH_low)
plt.plot(t, vz_from_absH_low)

#plt.plot(t, relH)
#plt.plot(t, vz_from_absH)

#tau = 3
#alpha = dt/(tau + dt)
#plt.plot(t, vz_from_a)
#diff = vz_from_a - vz_from_absHx

#plt.plot(t, medianfilter(vz_from_relH, 100))
#plt.plot(t, signal.filtfilt(b, a, vz_from_absH))
#plt.plot(t, signal.filtfilt(b, a, vz_from_a))
#plt.plot(t, signal.filtfilt(b, a, diff))
#plt.plot(t, signal.lfilter(bhigh, ahigh, vz_from_a))
