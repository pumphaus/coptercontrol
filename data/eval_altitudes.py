#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  9 21:51:49 2019

@author: pumphaus
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as signal
import json
from itertools import chain

data = {}

def isiterable(x):
    try:
        iter(x)
        return True
    except TypeError:
        return False

with open('pid_tune_climbrate_p_0.4.txt', 'r') as f:
    for line in f:
        field, sep, entries = line.strip().partition(',')
        d = json.loads('[' + entries + ']')

        if field not in data:
            data[field] = []
        data[field].extend(d)

#h = np.reshape(data['relativeAltitude'], (-1, 2))
climbRate = np.reshape(data['climbRate'], (-1, 2))

dt = np.mean(np.diff(climbRate[:, 0])) / 1e3
fs = 1/dt

plt.figure()
plt.plot(climbRate[:, 0], climbRate[:, 1])

f, Pxx = signal.periodogram(climbRate[:, 1], fs, 'hann')
plt.figure()
plt.plot(f, Pxx)
