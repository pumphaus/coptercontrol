#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  9 21:18:17 2019

@author: pumphaus
"""

import asyncio
import cbor2
from pywebchannel.pywebchannel.asynchronous import QWebChannel
import sys
from websockets.client import WebSocketClientProtocol, connect as ws_connect


class QWebChannelWebSocketProtocol(WebSocketClientProtocol):
    """ Bridges WebSocketClientProtocol and QWebChannel.
    Continuously reads messages in a task and invokes QWebChannel.message_received()
    for each. Calls QWebChannel.connection_open() when connected.
    Also patches QWebChannel.send() to run the websocket's send() in a task"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _task_send(self, data):
        data = cbor2.dumps(data)
        self.loop.create_task(self.send(data))

    def connection_open(self):
        super().connection_open()

        self.webchannel = QWebChannel()
        self.webchannel.send = self._task_send
        self.webchannel.connection_made(self)

        self.loop.create_task(self.read_msgs())

    async def read_msgs(self):
        async for msg in self:
            self.webchannel.message_received(cbor2.loads(msg))


def patched_send(self, data):
    # Don't encode as JSON; let the transport handle that
    self.transport.send(data)

QWebChannel.send = patched_send

async def main():
    print("Connecting...")
    # try:
    proto = await ws_connect(sys.argv[1], create_protocol=QWebChannelWebSocketProtocol)
    channel = proto.webchannel
    await channel
    tuning = channel.objects["tuning"]
    tuning.propertySamplesReady.connect(lambda x: print(x))

    while True:
        await asyncio.sleep(1)

if __name__ == '__main__':
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("Quit.")
