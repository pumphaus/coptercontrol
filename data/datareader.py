#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 10 20:24:23 2020

@author: pumphaus
"""

import numpy as np
import cbor
import struct
import sys
import gzip
import textwrap
import matplotlib.pyplot as plt


def readMessages(buf):
    while len(buf) > 4:
        size, = struct.unpack(">I", buf[:4])
        if len(buf) - 4 < size:
            break

        data = buf[4:4+size]
        buf = buf[4+size:]
        data = cbor.loads(data)
        yield data


def print_fields(buf):
    all_keys = {}
    for x in readMessages(buf):
        for key in x.keys():
            all_keys[key] = 1

    print("Data fields:")
    print(textwrap.indent('\n'.join(sorted(all_keys.keys())), '  '))


def print_data(buf, field):
    for msg in readMessages(buf):
        values = msg.get(field, [])
        if len(values) > 0:
            print(values)


def get_data(buf, fields):
    data = {}

    for msg in readMessages(buf):
        for field in fields:
            for item in msg.get(field, []):
                t, value = item
                if not hasattr(value, '__iter__'):
                    data.setdefault(field, []).append([t, value])
                else:
                    for i in range(len(value)):
                        data.setdefault(field + '_' + str(i), []).append([t, value[i]])

    for field in data.keys():
        data[field] = np.array(data[field])

    return data


def plot_data(buf, fields):
    data = get_data(buf, fields)

    for name, d in data.items():
        plt.plot(d[:, 0], d[:, 1])

    plt.legend(data.keys())
    plt.show()


if __name__ == '__main__':
    fname = sys.argv[1]
    with gzip.open(fname, mode='rb') as file:
        buf = file.read()

    if len(sys.argv) == 2:
        print_fields(buf)
    else:
        plot_data(buf, sys.argv[2:] )
