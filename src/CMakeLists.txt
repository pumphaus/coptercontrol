add_subdirectory(control)

if (COPTERCONTROL_SIMULATOR)
    add_subdirectory(tuner)
    add_subdirectory(remote)
endif()
