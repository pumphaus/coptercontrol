import QtQml.Models
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Page {
    id: page

    title: "PID Tuning"

    required property var tuning
    required property string pidName
    required property int index

    property var params
    property bool complete: false

    function updateParams() {
        if (!page) {
            return;
        }
        page.params = page.tuning.pidParams[pidName][index]
    }
    function setParam(param, value) {
        console.log(`SET ${pidName}[${index}].${param} = ${value}`)
        page.tuning.setPidParam(pidName, index, param, value)
    }
    Component.onCompleted: {
        page.tuning.pidParamsChanged.connect(updateParams)
        page.updateParams()
        page.complete = true
    }
    Component.onDestruction: {
        page.tuning.pidParamsChanged.disconnect(updateParams)
    }

    ListModel {
        id: properties

        ListElement { name: "derivTime"; decimals: 4 }
        ListElement { name: "intFrequency"; decimals: 2 }
        ListElement { name: "intLimit"; decimals: 2 }
        ListElement { name: "intTime"; decimals: 3}
        ListElement { name: "outputBandwidth"; decimals: 2 }
        ListElement { name: "outputLimit"; decimals: 2 }
        ListElement { name: "outputValue"; decimals: 4; readonly: true }
        ListElement { name: "pGain"; decimals: 4 }
        ListElement { name: "processValue"; decimals: 4; readonly: true }
        ListElement { name: "setpoint"; decimals: 4; readonly: true }
        ListElement { name: "setpointBandwidth"; decimals: 2 }
    }

    ScrollView {
        anchors.fill: parent
        ListView {
            model: page.complete ? properties : null
            spacing: 6
            delegate: Item {
                id: del
                required property string name
                required property int decimals
                required property bool readonly

                readonly property double value: params[name] || 0

                width: parent.width
                height: {
                    let h = 0
                    for (let child of children) {
                        h = Math.max(child.height, h)
                    }
                    h
                }

                Label {
                    id: nameLabel
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.horizontalCenter
                    anchors.rightMargin: 6
                    text: `${name}:`
                    font.bold: true
                }
                Label {
                    id: currentValueLabel
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.horizontalCenter
                    anchors.leftMargin: 6
                    text: value.toFixed(decimals)
                }
                DoubleSpinBox {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: currentValueLabel.right
                    anchors.leftMargin: 6

                    property bool complete: false
                    visible: !del.readonly
                    decimals: del.decimals
                    realFrom: -999999
                    realTo:    999999
                    stepSize: 1
                    onRealValueChanged: if (complete) page.setParam(del.name, realValue)
                    Component.onCompleted: {
                        value = decimalToInt(del.value)
                        complete = true
                    }
                }
            }
        }
    }

}
