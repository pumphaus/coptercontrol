import QtQuick
import QtQuick.Controls

SpinBox {
    id: spinBox

    property real realFrom
    property real realTo
    property int decimals: 2
    readonly property real realValue: value / decimalFactor
    readonly property int decimalFactor: Math.pow(10, decimals)

    from: decimalToInt(realFrom)
    to: decimalToInt(realTo)
    stepSize: decimalFactor
    property int largeStepSize: 10 * stepSize
    editable: true

    Keys.onPressed: event => {
                        if (event.key === Qt.Key_PageUp) {
                            value += largeStepSize
                        } else if (event.key === Qt.Key_PageDown) {
                            value -= largeStepSize
                        }
                    }

    function decimalToInt(decimal) {
        return Math.round(decimal * decimalFactor)
    }

    validator: DoubleValidator {
        bottom: Math.min(spinBox.from, spinBox.to)
        top:  Math.max(spinBox.from, spinBox.to)
        decimals: spinBox.decimals
        notation: DoubleValidator.StandardNotation
    }

    textFromValue: function(value, locale) {
        return Number(value / decimalFactor).toLocaleString(locale, 'f', spinBox.decimals)
    }

    valueFromText: function(text, locale) {
        return Math.round(Number.fromLocaleString(locale, text) * decimalFactor)
    }
}
