#ifndef CBORUTILS_H
#define CBORUTILS_H

#include <QObject>
#include <QQmlEngine>
#include <QJsonValue>

class CborUtils : public QObject
{
    Q_OBJECT
    QML_NAMED_ELEMENT(CBOR)
    QML_SINGLETON
public:
    explicit CborUtils(QObject *parent = nullptr);

    Q_INVOKABLE QByteArray toCbor(const QJSValue &value);
    Q_INVOKABLE QJsonValue fromCbor(const QByteArray &data);
};

#endif // CBORUTILS_H
