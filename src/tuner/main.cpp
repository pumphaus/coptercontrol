#include <QGuiApplication>
#include <QQmlApplicationEngine>

int main(int argc, char **argv)
{
    qputenv("QT_QUICK_CONTROLS_CONF", ":/Tuner/qtquickcontrols2.conf");

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine(QUrl("qrc:/Tuner/main.qml"));
    if (engine.rootObjects().isEmpty()) {
        return 1;
    }

    return app.exec();
}
