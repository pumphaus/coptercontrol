import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtWebSockets

Page {
    id: page

    property alias host: hostField.text
    required property WebSocket websocket

    GridLayout {
        anchors.centerIn: parent
        flow: GridLayout.LeftToRight
        columns: 2

        Label {
            text: qsTr("Host:")
        }
        TextField {
            id: hostField
            text: "localhost"
        }

        Button {
            Layout.alignment: Qt.AlignRight
            Layout.columnSpan: 2
            text: qsTr("Connect")
            onClicked: {
                page.websocket.active = false
                page.websocket.url = `ws://${page.host}:7522`
                page.websocket.active = true
            }
        }
    }

}
