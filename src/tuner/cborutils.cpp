#include "cborutils.h"

#include <QCborValue>
#include <QJsonValue>

CborUtils::CborUtils(QObject *parent)
    : QObject{parent}
{

}

QByteArray CborUtils::toCbor(const QJSValue &value)
{
    return QCborValue::fromVariant(value.toVariant()).toCbor(
        QCborValue::EncodingOptions{QCborValue::UseFloat | QCborValue::UseIntegers});
}

QJsonValue CborUtils::fromCbor(const QByteArray &data)
{
    return QCborValue::fromCbor(data).toJsonValue();
}
