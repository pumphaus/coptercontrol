import QtQml.Models
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Page {
    id: page

    required property var tuning

    title: qsTr("PID Tuning")
    padding: 6

    ListModel {
        id: pidModel
        ListElement { text: "Roll";  section: "Angular Rate"; pidName: "angularRate"; index: 0 }
        ListElement { text: "Pitch"; section: "Angular Rate"; pidName: "angularRate"; index: 1 }
        ListElement { text: "Yaw";   section: "Angular Rate"; pidName: "angularRate"; index: 2 }

        ListElement { text: "Roll";  section: "Angle"; pidName: "absoluteAngle"; index: 0 }
        ListElement { text: "Pitch"; section: "Angle"; pidName: "absoluteAngle"; index: 1 }

        ListElement { text: "Climb rate";  section: "Climb rate"; pidName: "climbRate"; index: 0 }
    }

    ScrollView {
        anchors.fill: parent

        ListView {
            model: pidModel

            section.property: "section"
            section.delegate: Label {
                required property string section
                text: section
                font.bold: true
                font.pointSize: 12
                topPadding: 6
                bottomPadding: 6
            }

            delegate: ItemDelegate {
                required property var model
                required property string pidName
                required property int index
                required property string section
                leftPadding: 12
                text: model.text
                width: parent.width

                onClicked: page.StackView.view.push(
                               "PidTuner.qml", { tuning, pidName, index, title: `${section} - ${model.text}`})
            }
        }
    }

}
