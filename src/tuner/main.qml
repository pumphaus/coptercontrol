import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtWebSockets
import Tuner

import "qwebchannel.js" as WebChannel

ApplicationWindow {
    id: root

    visible: true
    property var channel

    WebSocket {
        id: socket

        function send(data) { sendBinaryMessage(CBOR.toCbor(data)) }
        property var onmessage
        onBinaryMessageReceived: message => onmessage({data: CBOR.fromCbor(message)})

        onStatusChanged: {
            switch (socket.status) {
            case WebSocket.Error:
                console.error(`Error: ${socket.errorString}`);
                root.channel = null
                break;
            case WebSocket.Closed:
                console.error(`Error: Socket at ${url} closed.`);
                root.channel = null
                break;
            case WebSocket.Open:
                console.info(`Connected to WebSocket at ${url}...`)
                const c = new WebChannel.QWebChannel(socket, function(ch) {
                    console.info(`Established QWebChannel connection.`)
                    root.channel = ch;
                    console.info(`  Objects: ${Object.keys(root.channel.objects)}`)
                });
                break;
            }
        }
    }

    onChannelChanged: if (root.channel) {
                          stack.push("OverviewPage.qml", {
                                         tuning: root.channel.objects.tuning
                                     })
                      } else {
                          stack.reset()
                      }

    header: ToolBar {
        RowLayout {
            ToolButton {
                text: "〈"
                visible: stack.depth > 1
                onClicked: stack.pop()
                font.bold: true
            }
            Label {
                text: stack.currentItem.title
                font.bold: true
            }
        }
    }

    StackView {
        id: stack
        anchors.fill: parent

        initialItem: ConnectionPage {
            websocket: socket
        }

        function reset() {
            while (depth > 1) pop()
        }
    }

}
