#include "flightregulator.h"

#include <armadillo>
#include <QMetaProperty>
#include <QSettings>
#include <QtDebug>

#include "pid.h"
#include "rotorcontroller.h"
#include "sensors.h"
#include "math_util.h"

// The signs might have to be inverted
const arma::fvec4 rollSign  { +1, -1,
                              +1, -1 };
const arma::fvec4 pitchSign { -1, -1,
                              +1, +1 };
const arma::fvec4 yawSign   { +1, -1,
                              -1, +1 };

namespace PIDConstants {
constexpr int MaxInterval = 3;  // ms
}

namespace RollPitchRatePIDs {

const auto [PGain, IntFrequency, DerivTime] = std::make_tuple(0.05f, 30.f, 0.002f);

constexpr float IntLimit = 1.;
constexpr float OutputLimit = std::numeric_limits<float>::infinity();
constexpr float InputBandwidth = 0;
constexpr float OutputBandwidth = 0;
}

namespace YawRatePID {
constexpr float PGain = 0.004f;
constexpr float IntTime = 0.5f;
constexpr float DerivTime = 0.f;

constexpr float IntLimit = 0;
constexpr float OutputLimit = 0.5;
constexpr float InputBandwidth = 0;
constexpr float OutputBandwidth = 0;
}

namespace AbsoluteAnglePIDs
{
constexpr float PGain = 0.f;
constexpr float IntTime = 0;
constexpr float DerivTime = 0;

constexpr float IntLimit = std::numeric_limits<float>::infinity();
constexpr float OutputLimit = 0;
constexpr float InputBandwidth = 0;
constexpr float OutputBandwidth = 0;
}

namespace  ClimbRatePID
{

const auto [PGain, IntTime, DerivTime] = zieglerNichols<TypePID_SomeOvershoot>(0.4, 1/1.3);
//constexpr float PGain = 0.4f;
//constexpr float IntTime = 0.f;
//constexpr float DerivTime = 0;

constexpr float IntLimit = 0;
constexpr float OutputLimit = 0.5f;
constexpr float OutputBandwidth = 0;
}

struct LowPassFilteredDerivator
{
    Derivative<float> deriv;
    ButterworthFilter<float> filter { 2 };

    explicit LowPassFilteredDerivator(float tau) {
        filter.tau = tau;
    }

    float operator()(float input, float dt)
    {
        return filter(deriv(input, dt), dt);
    }
};

constexpr double fc2tau(double fc)
{
    return 1.0 / (2 * M_PI * fc);
}

namespace {

QVariantMap gadgetToMap(const QMetaObject *metaObject, const void *gadget)
{
    QVariantMap map;
    for (int i = 0; i < metaObject->propertyCount(); ++i) {
        const auto prop = metaObject->property(i);
        map[prop.name()] = prop.readOnGadget(gadget);
    }
    return map;
}

const QString AngularRatePidName = QStringLiteral("angularRate");
const QString AbsoluteAnglePidName = QStringLiteral("absoluteAngle");
const QString ClimbRatePidName = QStringLiteral("climbRate");

}

FlightRegulator::FlightRegulator(QObject *parent)
    : QObject(parent)
{
    for (auto idx : { RollIndex, PitchIndex }) {
        auto &pid = s.angularRatePids[idx];
        pid.setPGain(RollPitchRatePIDs::PGain);
        pid.setIntFrequency(RollPitchRatePIDs::IntFrequency);
        pid.setDerivTime(RollPitchRatePIDs::DerivTime);
        pid.setOutputLimit(RollPitchRatePIDs::OutputLimit);
        pid.setIntLimit(RollPitchRatePIDs::IntLimit);
        pid.setDerivativeProvider(LowPassFilteredDerivator(fc2tau(20)));
        pid.setOutputBandwidth(RollPitchRatePIDs::InputBandwidth);
        pid.setOutputBandwidth(RollPitchRatePIDs::OutputBandwidth);
    }

    /* Yaw PID */ {
        auto &pid = s.angularRatePids[YawIndex];
        pid.configure(YawRatePID::PGain, YawRatePID::IntTime, YawRatePID::DerivTime);
        pid.setOutputLimit(YawRatePID::OutputLimit);
        pid.setIntLimit(YawRatePID::IntLimit);
        pid.setDerivativeProvider(LowPassFilteredDerivator(fc2tau(20)));
        pid.setOutputBandwidth(YawRatePID::InputBandwidth);
        pid.setOutputBandwidth(YawRatePID::OutputBandwidth);
    }

    for (auto &pid : s.absoluteAnglePids) {
        pid.configure(AbsoluteAnglePIDs::PGain, AbsoluteAnglePIDs::IntTime,
                       AbsoluteAnglePIDs::DerivTime);
        pid.setOutputLimit(AbsoluteAnglePIDs::OutputLimit);
        pid.setIntLimit(AbsoluteAnglePIDs::IntLimit);
        pid.setSetpointBandwidth(AbsoluteAnglePIDs::InputBandwidth);
        pid.setOutputBandwidth(AbsoluteAnglePIDs::OutputBandwidth);
    }

    s.climbRatePid.configure(ClimbRatePID::PGain, ClimbRatePID::IntTime, ClimbRatePID::DerivTime);  // ????
    s.climbRatePid.setIntLimit(ClimbRatePID::IntLimit);
    s.climbRatePid.setOutputLimit(ClimbRatePID::OutputLimit);
    s.climbRatePid.setOutputBandwidth(ClimbRatePID::OutputBandwidth);

    setPidDerivativesBySensors();

    // Debug dt
    connect(this, &FlightRegulator::updateComplete, this,
            [this, elapsed=ElapsedTimer(), i=0, dt=arma::running_stat<double>()]() mutable
    {
        if (!property("debug").toBool()) {
            return;
        }

        if (!elapsed.isValid()) {
            elapsed.start();
        }
        const float curDt = elapsed.nsecsElapsed() * 1e-6;
        dt(curDt);
        elapsed.restart();
        ++i;
        if (i >= 100) {
            i = 0;
            qDebug() << "FR Mean dt:" << dt.mean();
            dt.reset();
        }
    });
}

void FlightRegulator::setRotorController(RotorController *rotors)
{
    if (m_rotorController == rotors) {
        return;
    }

    m_rotorController = rotors;
}

void FlightRegulator::setSensors(Sensors *sensors)
{
    if (m_sensors == sensors) {
        return;
    }

    m_sensors = sensors;

    connect(m_sensors, &Sensors::updateComplete, this, &FlightRegulator::update);
    connect(m_sensors, &Sensors::climbRateChanged, this, &FlightRegulator::updateClimbRateOutput);
}

bool FlightRegulator::isActive() const
{
    return s.active;
}

arma::fvec3 FlightRegulator::angularRatePidOutput() const
{
    return s.angularRatePidOutput;
}

arma::fvec2 FlightRegulator::anglePidOutput() const
{
    return s.absoluteAnglePidOutput;
}

float FlightRegulator::climbRatePidOutput() const
{
    return s.climbRatePidOutput;
}

float FlightRegulator::climbRateSetpoint() const
{
    return s.climbRatePid.setpoint();
}

arma::fvec2 FlightRegulator::angleSetpoint() const
{
    return s.angleSetpoint;
}

arma::fvec3 FlightRegulator::angularRateSetpoint() const
{
    return s.angularRateSetpoint;
}

void FlightRegulator::copyStateTo(FlightRegulator *other)
{
    other->s = s;
    other->setPidDerivativesBySensors();
    other->m_rotorController->setThrottle(m_rotorController->throttle());
    other->m_rotorController->setEnabled(m_rotorController->isEnabled());
}

QVariantMap FlightRegulator::pidParams() const
{
    return QVariantMap{
        {AngularRatePidName, QVariantList{
            gadgetToMap(&PID::staticMetaObject, &s.angularRatePids[0]),
            gadgetToMap(&PID::staticMetaObject, &s.angularRatePids[1]),
            gadgetToMap(&PID::staticMetaObject, &s.angularRatePids[2]),
        }},
        {AbsoluteAnglePidName, QVariantList{
            gadgetToMap(&PID::staticMetaObject, &s.absoluteAnglePids[0]),
            gadgetToMap(&PID::staticMetaObject, &s.absoluteAnglePids[1]),
        }},
        {ClimbRatePidName, QVariantList{
            gadgetToMap(&PID::staticMetaObject, &s.climbRatePid),
        }},
    };
}

bool FlightRegulator::setPidParam(const QString &pidName, int subIndex,
                                  const QByteArray &paramName, const QVariant &value)
{
    PID *pid = [&]() -> PID* {
        try {
            if (pidName == AngularRatePidName) {
                return &s.angularRatePids.at(subIndex);
            } else if (pidName == AbsoluteAnglePidName) {
                return &s.absoluteAnglePids.at(subIndex);
            } else if (pidName == ClimbRatePidName) {
                return &s.climbRatePid;
            }
        } catch (...) {}
        return nullptr;
    }();

    if (!pid) {
        qWarning() << "setPidParam: Unknown PID" << pidName << subIndex;
        return false;
    }

    const auto prop = PID::staticMetaObject.property(
                PID::staticMetaObject.indexOfProperty(paramName));
    return prop.writeOnGadget(pid, value);
}

void FlightRegulator::storeSettings(QSettings &settings) const
{
    const auto storePids = [&](auto &&name, auto &&pids) {
        settings.beginWriteArray(name);
        for (size_t i = 0; i < pids.size(); ++i) {
            const PID &pid = pids[i];
            settings.setArrayIndex(i);
            pid.storeSettings(settings);
        }
        settings.endArray();
    };

    storePids("pid/angularRate", s.angularRatePids);
    storePids("pid/absoluteAngle", s.absoluteAnglePids);
    settings.beginGroup("pid/climbRate");
    s.climbRatePid.storeSettings(settings);
    settings.endGroup();
}

void FlightRegulator::restoreSettings(QSettings &settings)
{
    const auto restorePids = [&](auto &&name, auto &&pids) {
        settings.beginReadArray(name);
        for (size_t i = 0; i < pids.size(); ++i) {
            PID &pid = pids[i];
            settings.setArrayIndex(i);
            pid.restoreSettings(settings);
        }
        settings.endArray();
    };

    restorePids("pid/angularRate", s.angularRatePids);
    restorePids("pid/absoluteAngle", s.absoluteAnglePids);
    settings.beginGroup("pid/climbRate");
    s.climbRatePid.restoreSettings(settings);
    settings.endGroup();

}

FlightRegulator::Mode FlightRegulator::mode() const
{
    return s.mode;
}

void FlightRegulator::setMode(FlightRegulator::Mode newMode)
{
    if (s.mode == newMode) {
        return;
    }

    s.mode = newMode;
    emit modeChanged(s.mode);
}

void FlightRegulator::setActive(bool active)
{
    if (s.active == active) {
        return;
    }

    s.active = active;
    m_rotorController->setEnabled(active);
    if (active) {
        for (auto &pid : s.angularRatePids) {
            pid.reset();
        }

        for (auto &pid : s.absoluteAnglePids) {
            pid.reset();
        }

        s.climbRatePid.reset();
    }
    emit activeChanged(s.active);
}

void FlightRegulator::setClimbRateSetpoint(float climbRateSetpoint)
{
    s.climbRatePid.setSetpoint(climbRateSetpoint);
    emit climbRateSetpointChanged(climbRateSetpoint);
}

void FlightRegulator::setAngleSetpoint(const arma::fvec2 &angleSetpoint)
{
    if (all(s.angleSetpoint == angleSetpoint))
        return;

    s.angleSetpoint = angleSetpoint;
    emit angleSetpointChanged(s.angleSetpoint);
}

void FlightRegulator::setAngularRateSetpoint(const arma::fvec3 &angularRateSetpoint)
{
    if (all(s.angularRateSetpoint == angularRateSetpoint))
        return;

    s.angularRateSetpoint = angularRateSetpoint;
    emit angularRateSetpointChanged(s.angularRateSetpoint);
}

void FlightRegulator::update()
{
    if (!s.active) {
        return;
    }

    using namespace arma;

    const auto omega = m_sensors->angularVelocity();
    const auto angles = m_sensors->angles();

    for (size_t i = 0; i < s.absoluteAnglePids.size(); ++i) {
        auto &pid = s.absoluteAnglePids[i];
        pid.setSetpoint(s.angleSetpoint[i]);
        pid.pushProcessValue(angles[i]);
        s.absoluteAnglePidOutput[i] = pid.outputValue();
    }

    for (size_t i = 0; i < s.angularRatePids.size(); ++i) {
        auto &pid = s.angularRatePids[i];
        if (i < s.absoluteAnglePidOutput.size()) {
            pid.setSetpoint(s.mode == RateMode ? s.angularRateSetpoint[i]
                                               : s.absoluteAnglePidOutput[i]);
        }
        if (i == YawIndex) {
            pid.setSetpoint(s.angularRateSetpoint[i]);
        }
        pid.pushProcessValue(omega[i]);
        s.angularRatePidOutput[i] = pid.outputValue();
    }

    const fvec4 throttle = clamp(
              rollSign * s.angularRatePidOutput[RollIndex]
            + pitchSign * s.angularRatePidOutput[PitchIndex]
            + yawSign * s.angularRatePidOutput[YawIndex]
            + s.climbRatePidOutput
            + 0.3f,
            0, 1.f);

    m_rotorController->setThrottle(throttle);

    emit angularRatePidOutputChanged(s.angularRatePidOutput);
    emit anglePidOutputChanged(s.absoluteAnglePidOutput);
    emit climbRatePidOutputChanged(s.climbRatePidOutput);

    emit updateComplete();
}

void FlightRegulator::updateClimbRateOutput()
{
    s.climbRatePid.pushProcessValue(m_sensors->climbRate());
    s.climbRatePidOutput = s.climbRatePid.outputValue();
}

void FlightRegulator::setPidDerivativesBySensors()
{
    for (auto idx : { RollIndex, PitchIndex }) {
        auto &pid = s.absoluteAnglePids[idx];
        pid.setDerivativeProvider([=](float, float) {
            if (m_sensors) {
                return m_sensors->angularVelocity()[idx];
            }
            return 0.f;
        });
    }

    s.climbRatePid.setDerivativeProvider([=](float, float) {
        if (m_sensors) {
            return m_sensors->climbAcceleration();
        }
        return 0.f;
    });
}
