#include "tuninginterface.h"

#include "config.h"
#include "flightregulator.h"
#include "sensors.h"

#include <QBuffer>
#include <QDateTime>
#include <QDynamicPropertyChangeEvent>
#include <QMetaObject>
#include <QMetaProperty>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTimer>

#include <QWebChannelAbstractTransport>
#include <QWebChannel>
#include <QWebSocket>
#include <QWebSocketServer>

#include <QtDebug>

#include <functional>
#include <sstream>
#include <iomanip>

using namespace Qt::StringLiterals;

constexpr int MapSignalOffset = 1000;
static constexpr char SignalMapperPropName[] = "__statusBroadcasterSignalMapper";
static constexpr char DynaPropFilterPropName[] = "__dynaPropEventFilter";

static constexpr int WebChannelPort = 7522;

namespace {
QJsonValue toJson(const QVariant &variant)
{
    if (variant.canConvert<arma::fvec3>()) {
        auto vec = variant.value<arma::fvec3>();
        return QJsonArray {vec(0), vec(1), vec(2)};
    } else if (variant.canConvert<arma::fvec2>()) {
        auto vec = variant.value<arma::fvec2>();
        return QJsonArray {vec(0), vec(1) };
    } else if (variant.canConvert<arma::fmat33>()) {
        auto mat = variant.value<arma::fmat33>();
        return QJsonArray {
            QJsonArray { mat(0, 0), mat(0, 1), mat(0, 2) },
            QJsonArray { mat(1, 0), mat(1, 1), mat(1, 2) },
            QJsonArray { mat(2, 0), mat(2, 1), mat(2, 2) },
        };
    }
    return QJsonValue::fromVariant(variant);
}

qint64 now()
{
    return QDateTime::currentMSecsSinceEpoch();
}

struct LineProtocolValue {
    QVariant v;
    QByteArray name = "value"_ba;
};

QTextStream &operator<<(QTextStream &ts, const LineProtocolValue &value)
{
    switch (value.v.typeId()) {
    case QMetaType::Float:
    case QMetaType::Double: {
        ts << value.name << '='
           << QByteArray::number(value.v.toDouble(), 'e',
                                 std::numeric_limits<double>::max_digits10);
        return ts;
    } break;

    case QMetaType::Char:
    case QMetaType::Short:
    case QMetaType::Int:
    case QMetaType::Long:
    case QMetaType::LongLong: {
        ts << "value=" << value.v.toLongLong();
        return ts;
    } break;

    case QMetaType::UChar:
    case QMetaType::UShort:
    case QMetaType::UInt:
    case QMetaType::ULong:
    case QMetaType::ULongLong: {
        ts << value.name << '=' << value.v.toULongLong() << 'u';
        return ts;
    } break;

    case QMetaType::Bool: {
        ts << value.name << '=' << (value.v.toBool() ? "true" : "false");
        return ts;
    } break;
    }

    if (value.v.canConvert<QVariantList>()) {
        const auto list = value.v.toList();
        bool isFirst = true;
        for (int i = 0; i < list.size(); ++i) {
            if (!isFirst) {
                ts <<',';
            }
            isFirst = false;
            const auto value = list[i];
            ts << LineProtocolValue{value, "value"_ba + QByteArray::number(i)};
        }
        return ts;
    } else if (value.v.canConvert<arma::fvec>()) {
        const auto list = value.v.value<arma::fvec>();
        bool isFirst = true;
        for (int i = 0; i < list.size(); ++i) {
            const auto value = list[i];
            if (!isFirst) {
                ts <<',';
            }
            isFirst = false;
            ts << LineProtocolValue{value, "value"_ba + QByteArray::number(i)};
        }
        return ts;
    } else if (value.v.canConvert<QVariantMap>()) {
        const auto map = value.v.toMap();
        bool isFirst = true;
        for (const auto &[key, value] : map.asKeyValueRange()) {
            if (!isFirst) {
                ts <<',';
            }
            isFirst = false;
            ts << LineProtocolValue{value, key.toUtf8()};
        }
        return ts;
    }

    // Fallback: stringify it
    std::stringstream ss;
    ss << std::quoted(value.v.toString().toStdString());
    ts << value.name << '=' << ss.str().c_str();

    return ts;
}

Q_GLOBAL_STATIC(QNetworkAccessManager, qnam);

bool writeToInflux(const QUrl &influxUrl,
                   const QMap<QByteArray, QVector<TuningInterface::Sample>> &samples)
{
    const auto bucket = qEnvironmentVariable("INFLUXDB_BUCKET", "flightdata");
    const auto token = qEnvironmentVariable("INFLUXDB_TOKEN").toUtf8();
    const auto org = qEnvironmentVariable("INFLUXDB_ORGANIZATION");
    if (token.isEmpty()) {
        return false;
    }

    const auto unit =
#if COPTERCONTROL_SIMULATOR
        "simulator"_ba;
#else
        "quadcopter"_ba;
#endif

    QBuffer lineProtocolData;
    lineProtocolData.open(QIODevice::ReadWrite);
    QTextStream ts(&lineProtocolData);

    for (const auto &[key, samples] : samples.asKeyValueRange()) {
        for (const auto &sample : samples) {
            ts << key << ",unit=" << unit << ' ' << LineProtocolValue{sample.value}
               << ' ' << sample.timestamp << Qt::endl;
        }
    }

    lineProtocolData.seek(0);

    QUrl writeUrl = influxUrl;
    writeUrl.setPath(writeUrl.path() + "/api/v2/write");
    writeUrl.setQuery(u"org=%2&bucket=%1&precision=ms"_s.arg(bucket, org));

    QNetworkRequest req(writeUrl);
    req.setRawHeader("Authorization"_ba,
                     "Token "_ba + token);
    req.setRawHeader("Accept"_ba, "application/json"_ba);
    req.setRawHeader("Content-Type"_ba, "text/plain; charset=utf-8"_ba);

    auto *reply = qnam->post(req, lineProtocolData.buffer());
    QObject::connect(reply, &QNetworkReply::finished, reply, [reply] {
        const auto contents = reply->readAll();
        if (!contents.isEmpty()) {
            qDebug() << "Influx write response:" << contents.constData();
        }
        reply->deleteLater();
    });

    return true;
}

}

class Mapper : public QObject
{
public:
    std::function<void(int)> callback;

    explicit Mapper(QObject *parent = nullptr) : QObject(parent) {}

protected:
    int qt_metacall(QMetaObject::Call call, int id, void **o) override
    {
        if (call != QMetaObject::InvokeMetaMethod || id < MapSignalOffset) {
            return id;
        }

        callback(id - MapSignalOffset);
        return -1;
    }
};

void removePropertyNotifyMapper(QObject *object)
{
    if (!object) {
        return;
    }
    if (QObject *existingMapper = object->property(SignalMapperPropName).value<QObject*>()) {
        delete existingMapper;
    }
    object->setProperty(SignalMapperPropName, QVariant());
}

template<class T>
void installPropertyNotifyMapper(QObject *object, T callback)
{
    removePropertyNotifyMapper(object);

    Mapper *mapper = new Mapper;
    QObject::connect(object, &QObject::destroyed, mapper, &QObject::deleteLater);
    mapper->callback = std::move(callback);

    auto mo = object->metaObject();

    for (int i = mo->propertyOffset(); i < mo->propertyCount(); ++i) {
        const QMetaProperty prop = mo->property(i);
        if (!prop.hasNotifySignal()) {
            continue;
        }
        QMetaObject::connect(object, prop.notifySignalIndex(), mapper,
                             prop.propertyIndex() + MapSignalOffset,
                             Qt::DirectConnection);
    }

    object->setProperty(SignalMapperPropName, QVariant::fromValue<QObject*>(mapper));
}

class DynaPropFilter : public QObject
{
    Q_OBJECT
public:
    static DynaPropFilter *get(QObject *object)
    {
        if (!object) {
            return nullptr;
        }
        auto *filter = object->property(DynaPropFilterPropName).value<DynaPropFilter*>();
        if (!filter) {
            filter = new DynaPropFilter;
            filter->moveToThread(object->thread());
            filter->setParent(object);
            object->installEventFilter(filter);
        }
        return filter;
    }

    static void remove(QObject *object)
    {
        if (!object) {
            return;
        }

        auto *filter = object->property(DynaPropFilterPropName).value<DynaPropFilter*>();
        if (filter) {
            filter->deleteLater();
        }
    }

signals:
    void dynamicPropertySampleAcquired(
            const QByteArray &name, const TuningInterface::Sample &sample);

protected:
    bool eventFilter(QObject *watched, QEvent *event) override
    {
        if (event->type() == QEvent::DynamicPropertyChange) {
            auto *dpcEvent = static_cast<QDynamicPropertyChangeEvent*>(event);
            const QVariant value = watched->property(dpcEvent->propertyName());
            emit dynamicPropertySampleAcquired(dpcEvent->propertyName(), { now(), value });
        }

        return false;
    }
};

TuningInterface::TuningInterface(QObject *parent)
    : QObject(parent)
{
    qRegisterMetaType<Sample>();
    m_broadcastTimer = new QTimer(this);
    connect(m_broadcastTimer, &QTimer::timeout,
            this, &TuningInterface::sendUpdates);
    m_broadcastTimer->start(50);

    QMetaType::registerConverter<arma::fvec2, arma::fvec>();
    QMetaType::registerConverter<arma::fvec3, arma::fvec>();
    QMetaType::registerConverter<arma::fvec4, arma::fvec>();

    m_influxUrl = QUrl::fromUserInput(
        qEnvironmentVariable("INFLUXDB_URL", "http://localhost:8086"));
}

TuningInterface::~TuningInterface()
{
}

void TuningInterface::setSensors(Sensors *sensors)
{
    if (m_sensors == sensors) {
        return;
    }

    DynaPropFilter::remove(m_sensors);
    removePropertyNotifyMapper(m_sensors);
    m_sensors = sensors;

    if (!m_sensors) {
        return;
    }

    auto *filter = DynaPropFilter::get(m_sensors);
    connect(filter, &DynaPropFilter::dynamicPropertySampleAcquired,
            this, &TuningInterface::stashPropertySample);

    installPropertyNotifyMapper(m_sensors, [=](int property) {
        const auto prop = m_sensors->metaObject()->property(property);
        const Sample sample { now(), prop.read(m_sensors) };
        QMetaObject::invokeMethod(this, [=] {
            stashPropertySample(prop.name(), sample);
        });
    });
}

void TuningInterface::setFlightRegulator(FlightRegulator *regulator)
{
    if (m_regulator == regulator) {
        return;
    }

    DynaPropFilter::remove(m_regulator);
    removePropertyNotifyMapper(m_regulator);
    m_regulator = regulator;

    if (!m_regulator) {
        return;
    }

    auto *filter = DynaPropFilter::get(m_regulator);
    connect(filter, &DynaPropFilter::dynamicPropertySampleAcquired,
            this, &TuningInterface::stashPropertySample);

    installPropertyNotifyMapper(m_regulator, [=](int property) {
        const auto prop = m_regulator->metaObject()->property(property);
        const Sample sample { now(), prop.read(m_regulator) };
        QMetaObject::invokeMethod(this, [=] {
            stashPropertySample(prop.name(), sample);
        });
    });

    emit pidParamsChanged();
}

QVariantMap TuningInterface::pidParams() const
{
    if (!m_regulator) {
        return {};
    }
    return m_regulator->pidParams();
}

bool TuningInterface::setPidParam(const QString &pidName, int subIndex, const QByteArray &paramName, const QVariant &value)
{
    if (!m_regulator) {
        return false;
    }

    const auto ok = m_regulator->setPidParam(pidName, subIndex, paramName, value);
    if (ok) {
        emit pidParamsChanged();
    } else {
        qWarning() << "Failed to write" << pidName << subIndex << paramName << "=" << value;
    }
    return ok;
}

void TuningInterface::stashPropertySample(const QByteArray &name, const Sample &value)
{
    m_cache[name].append(value);
}

void TuningInterface::sendUpdates()
{
    if (m_cache.isEmpty()) {
        return;
    }

    writeToInflux(m_influxUrl, m_cache);

    QJsonObject object;
    for (auto it = m_cache.constBegin(); it != m_cache.constEnd(); ++it)
    {
        auto name = it.key();
        auto values = it.value();
        QJsonArray array;
        for (const auto &value : values) {
            QJsonArray subArray;
            subArray.append(value.timestamp);
            subArray.append(toJson(value.value));
            array.append(subArray);
        }
        object[name] = array;
    }
    m_cache.clear();

    emit pidParamsChanged();
    emit propertySamplesReady(object);
}

class CCTransport : public QWebChannelAbstractTransport
{
    Q_OBJECT
public:
    explicit CCTransport(QWebSocket *socket, QObject *parent = nullptr)
        : QWebChannelAbstractTransport(parent), m_socket(socket)
    {
        connect(m_socket, &QWebSocket::binaryMessageReceived,
                this, &CCTransport::handleMessage);
    }

    void sendMessage(const QJsonObject &message) override
    {
        auto cbor = QCborValue::fromJsonValue(message);
        const auto bytes = cbor.toCbor(QCborValue::EncodingOptions{QCborValue::UseFloat | QCborValue::UseIntegers});
        m_socket->sendBinaryMessage(bytes);
    }

    void handleMessage(const QByteArray &bytes)
    {
        const auto cbor = QCborValue::fromCbor(bytes);
        emit messageReceived(cbor.toJsonValue().toObject(), this);
    }

private:
    QWebSocket *m_socket = nullptr;
};

QWebChannel *createWebChannel(QObject *parent = nullptr)
{
    auto *channel = new QWebChannel(parent);
    auto *server = new QWebSocketServer("coptercontrol", QWebSocketServer::NonSecureMode,
                                        parent);
    QObject::connect(server, &QWebSocketServer::newConnection, server,
                     [=]
    {
        auto *socket = server->nextPendingConnection();
        auto *transport = new CCTransport(socket, socket);
        QObject::connect(socket, &QWebSocket::disconnected, socket, &QObject::deleteLater);
        channel->connectTo(transport);
    });

    if (!server->listen(QHostAddress::Any, WebChannelPort)) {
        qWarning() << "Failed to start server on" << WebChannelPort << ":"
                   << server->errorString();
    } else {
        qInfo() << "WebChannel server listening on" << server->serverUrl();
    }
    return channel;
}

#include "tuninginterface.moc"
