#include <QByteArray>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QLoggingCategory>
#include <QtEndian>
#include <QtDebug>

#include <linux/spi/spidev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

Q_LOGGING_CATEGORY(voltmon, "voltmon");

constexpr char SpiDevFile[] = "/dev/spidev0.0";

constexpr double Vref = 3.3;
constexpr double R1 = 20000;
constexpr double R2 = 4700;
constexpr double ScaleFactor = R2 / (R1 + R2);

int SpiMode = SPI_MODE_0;
int fd = -1;

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addOptions({
        { {"verbose", "v"}, "Be verbose" }
    });
    parser.addPositionalArgument("channel", "channel number (default: 0)");
    parser.process(app);

    if (parser.isSet("verbose")) {
        QLoggingCategory::setFilterRules("voltmon.debug=true");
    } else {
        QLoggingCategory::setFilterRules("voltmon.debug=false");
    }

    int channel = parser.positionalArguments().value(0).toInt();
    channel = qBound(0, channel, 7);

    fd = open(SpiDevFile, O_RDWR);
    atexit([]() { close(fd); });

    if (fd < 0) {
        qFatal("Failed to open device!");
    }

    QByteArray tx(3, Qt::Uninitialized);
    tx[0] = 1;
    quint16 command = (1 << 15) | (channel << 12);
    qToBigEndian(command, tx.data() + 1);

    qCDebug(voltmon) << "Get channel" << channel;
    qCDebug(voltmon) << "Transfer" << tx.toHex(':');

    QByteArray rx(3, '\0');

    if (ioctl(fd, SPI_IOC_WR_MODE, &SpiMode) < 0) {
        qFatal("Failed to set SPI mode!");
    }

    spi_ioc_transfer transfer;
    memset(&transfer, 0, sizeof transfer);

    transfer.tx_buf = reinterpret_cast<quint64>(tx.data());
    transfer.rx_buf = reinterpret_cast<quint64>(rx.data());
    transfer.len = tx.size();
    transfer.speed_hz = 1000000;
    transfer.cs_change = true;
    transfer.bits_per_word = 8;

    if (ioctl(fd, SPI_IOC_MESSAGE(1), &transfer) < 0) {
        qFatal("Failed to sample voltage!");
    }

    const quint16 value = qFromBigEndian<quint16>(rx.data() + 1) & 0x3FF;

    const double voltage = value / 1024.0 * Vref / ScaleFactor;
    qCDebug(voltmon) << "Received:" << rx.toHex(':');

    printf("%.2f\n", voltage);
}
