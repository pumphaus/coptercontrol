#ifndef MATH_UTIL_H
#define MATH_UTIL_H

#include <armadillo>
#include <deque>
#include <QDataStream>
#include <QtDebug>
#include "elapsedtimer.h"
#include <cmath>

template<class T>
QDebug operator<<(QDebug debug, const arma::Col<T> &col)
{
    QDebugStateSaver state(debug);
    debug.nospace() << "arma::Col(";

    for (size_t i = 0; i < col.size(); ++i) {
        if (i != 0) debug << ",";
        debug << col[i];
    }

    debug << ')';
    return debug;
}

template<class T>
QDebug operator<<(QDebug debug, const arma::Row<T> &col)
{
    QDebugStateSaver state(debug);
    debug.nospace() << "arma::Row(";

    for (size_t i = 0; i < col.size(); ++i) {
        if (i != 0) debug << ",";
        debug << col[i];
    }

    debug << ')';
    return debug;
}

template<class T>
QDebug operator<<(QDebug debug, const arma::Mat<T> &mat)
{
    QDebugStateSaver state(debug);
    debug.nospace() << "arma::Mat(\n";
    std::stringstream ss;
    ss << mat;
    debug << ss.str().c_str() << ')';
    return debug;
}

template<class T>
QDataStream &operator<<(QDataStream &stream, const arma::Mat<T> &mat)
{
    std::stringstream ss;
    mat.save(ss, arma::csv_ascii);
    stream << QByteArray::fromStdString(ss.str());
    return stream;
}

template<class T>
QDataStream &operator>>(QDataStream &stream, arma::Mat<T> &mat)
{
    QByteArray ba;
    stream >> ba;
    std::stringstream ss(ba.toStdString());
    mat.load(ss, arma::csv_ascii);
    return stream;
}

template<class T>
auto quantile(T v, float q) -> typename T::value_type
{
    const size_t pos = q * v.size();
    auto iter = v.begin();
    std::advance(iter, pos);
    std::nth_element(v.begin(), iter, v.end());
    return *iter;
}

template<class T>
auto median(const T &v) -> typename T::value_type
{
    return quantile(v, 0.5f);
}

template<class T>
struct RunningAverage
{
    T avg {};
    unsigned int N = 0;

    T update(const T &newValue) {
        avg = (N * avg + newValue) / (N + 1);
        ++N;
        return get();
    }

    T get() const { return avg; }

    T operator()(const T &value) { return update(value); }
    operator T() const { return get(); }
};

struct EllipsoidFitResult
{
    arma::vec center;
    arma::vec radii;
    arma::mat eigenVectors;
};

EllipsoidFitResult ellipsoidFit(const arma::mat &X);

Q_DECLARE_METATYPE(arma::fvec)
Q_DECLARE_METATYPE(arma::fvec2)
Q_DECLARE_METATYPE(arma::fvec3)
Q_DECLARE_METATYPE(arma::fvec4)
Q_DECLARE_METATYPE(arma::fmat)
Q_DECLARE_METATYPE(arma::fmat33)

template<class T>
struct Derivative
{
    T prev {};
    T dx = 1;

    explicit Derivative(T dx = 1) : dx(dx) {}

    T operator()(T value) {
        const float dv = value - prev;
        prev = value;
        if (qIsNull(dx)) {
            qDebug() << "Crap, dx is 0!";
            return 0;
        }
        return dv / dx;
    }

    T operator()(T value, T dx) {
        this->dx = dx;
        return operator()(value);
    }
};

template<class T>
struct TimedDerivative
{
    ElapsedTimer elapsed;
    Derivative<T> deriv;

    T operator()(T value) {
        if (!elapsed.isValid()) {
            elapsed.start();
            deriv(value);
            return 0;
        }

        deriv.dx = elapsed.nsecsElapsed() * 1e-9;
        elapsed.restart();
        return deriv(value);
    }
};

template<class T>
struct MedianFilter
{
    std::deque<T> inWindow;
    int winSize;

    explicit MedianFilter(int winSize = 5) : winSize(winSize) {}

    T operator()(T value) {
        while (inWindow.size() >= winSize) {
            inWindow.erase(inWindow.begin());
        }
        inWindow.push_back(value);

        return median(inWindow);
    }
};


// "Inter-Quantile-Range" Filter for outlier removal
template<class T>
struct IQRFilter
{
    std::deque<T> histWindow;
    int winSize;
    float q;
    float thres;
    T latestGoodValue = 0;

    explicit IQRFilter(int winSize = 10, float quantile = .25f, float thres = 1.5f)
        : winSize(winSize), q(quantile), thres(thres)
    {
        reset();
    }

    void reset(T value = 0) {
        histWindow.clear();
        histWindow.push_back(value);
        latestGoodValue = value;
    }

    T operator()(T value) {
        // If the history is all non-finite, reset the filter with the current value
        if (!std::any_of(histWindow.begin(), histWindow.end(),
                         [](auto v) { return std::isfinite(v); }))
        {
            reset(value);
            return value;
        }

        const auto Q1 = quantile(histWindow, q);
        const auto Q3 = quantile(histWindow, 1 - q);
        const auto IQR = Q3 - Q1;

        auto retval = value;

        // Check if the value is within the desired range. If so, make it the
        // latest "good" value.
        if ((Q1 - thres * IQR) < value && value < (Q3 + thres * IQR)) {
            latestGoodValue = value;
        } else {
            // Else, drop the value and use the last good one, instead.
            retval = latestGoodValue;
        }

        // Update the history
        while (histWindow.size() >= winSize) {
            histWindow.erase(histWindow.begin());
        }
        histWindow.push_back(value);

        return retval;
    }
};

template<class T>
struct RCFilter
{
    T last {};
    T tau = {};
    ElapsedTimer elapsed;

    explicit RCFilter(T tau = 0.1)
        : tau(tau) {}
    explicit RCFilter(const std::chrono::nanoseconds &tau)
        : tau(tau.count() * 1e-9) {}

    T operator()(T value, T dt)
    {
        if (!elapsed.isValid()) {
            elapsed.start();
            last = value;
            return value;
        }

        const T alpha = dt / (tau + dt);
        const T filtered = (1 - alpha) * last + alpha * value;
        last = filtered;
        elapsed.restart();
        return filtered;
    }

    T operator()(T value)
    {
        if (!elapsed.isValid()) {
            elapsed.start();
            last = value;
            return value;
        }

        const T dt = T(elapsed.nsecsElapsed() * 1e-9);
        return operator()(value, dt);
    }
};

template<class T, size_t Size = 1>
struct IIRFilter
{
    arma::Mat<T> y;
    arma::Mat<T> x;
    arma::Col<T> b;
    arma::Col<T> a;

    bool isFirst = true;

    explicit IIRFilter(int order)
        : y(order + 1, Size, arma::fill::zeros), x(order + 1, Size, arma::fill::zeros)
    {
    }
    explicit IIRFilter(const arma::Col<T> &b, const arma::Col<T> &a)
        : b(b), a(a), y{a.size(), Size, arma::fill::zeros}, x{b.size(), Size, arma::fill::zeros}
    {
    }

    T operator()(T value)
    {
        static_assert (Size == 1);
        typename arma::Col<T>::template fixed<Size> col;
        col[0] = value;
        return (*this)(col)[0];
    }

    typename arma::Col<T>::template fixed<Size> operator()(const typename arma::Col<T>::template fixed<Size> &value)
    {
        using namespace  arma;

        if (isFirst) {
            x.each_row() = value.t();
            y.each_row() = value.t();
            isFirst = false;
            return y.row(0).t();
        }

        x = shift(x, 1);
        y = shift(y, 1);
        x.row(0) = value.t();

        for (size_t i = 0; i < Size; ++i) {
            y(0, i) = ( dot(b, x(span::all, i)) - dot(a(span(1, a.size() - 1)),
                                                      y(span(1, y.n_rows - 1), i)) ) / a[0];
        }

        return y.row(0).t();
    }
};

enum FilterType {
    TypeLowpass,
    TypeHighpass,
    TypeHighpass_DeltaInput
};

// https://stackoverflow.com/questions/20924868/calculate-coeWnicients-of-2nd-order-butterworth-low-pass-filter
// The sign of a[1] and a[2] has been flipped to match numpy output.
template<class T>
std::tuple<arma::Col<T>, arma::Col<T>> butter(int order, T Wn, FilterType type = TypeLowpass)
{
    if (order != 2) {
        qFatal("Only supports 2nd order butterworth for now.");
    }

    const double ita = 1.0 / std::tan(M_PI * Wn);
    const double q = std::sqrt(2.0);

    arma::Col<T> b(3);
    arma::Col<T> a(3);

    b[0] = 1.0 / (1.0 + q * ita + ita * ita);
    b[1] = 2 * b[0];
    b[2] = b[0];
    a[0] = 1.0;
    a[1] = -2.0 * (ita * ita - 1.0) * b[0];
    a[2] = (1.0 - q * ita + ita * ita) * b[0];

    if (type >= TypeHighpass) {
        b *= ita * ita;
        b[1] *= -1;
    }
    if (type == TypeHighpass_DeltaInput) {
        b[1] = -b[0];
        b.resize(2);
    }

    return std::make_tuple(b, a);
}

template<class T>
struct ButterworthFilter
{
    T tau;
    int order;
    IIRFilter<T> filter;
    FilterType type = TypeLowpass;

    explicit ButterworthFilter(int order, T tau = 1, FilterType type = TypeLowpass)
        : tau(tau), order(order), filter{order}, type{type}
    {
    }

    T operator()(T value, T dt) {
        if (qIsNull(dt)) {
            qDebug() << "Push initial filter value" << value;
            return filter(value);
        }

        const T fs = T(1) / dt;
        const T fc = 1.0 / (2 * M_PI * tau);
        const auto Wn = fc / fs;

        std::tie(filter.b, filter.a) = butter(order, Wn, type);

        return filter(value);
    }
};

template<class T>
struct TimedButterworthFilter
{
    ButterworthFilter<T> filter;
    ElapsedTimer elapsed;

    explicit TimedButterworthFilter(int order, T tau = 1)
        : filter{order, tau}
    {
    }

    T operator()(T value) {
        if (!elapsed.isValid()) {
            elapsed.start();
            return filter(value, 0);
        }

        const T dt = elapsed.nsecsElapsed() * 1e-9;
        elapsed.restart();

        return filter(value, dt);
    }
};

#endif // MATH_UTIL_H
