#include "pid.h"
#include "math_util.h"

#include <QMetaProperty>
#include <QSettings>

PID::PID()
{
    setDerivativeProvider({});
}

PID::PID(const PID &other) = default;

float PID::pGain() const
{
    return m_pGain;
}

float PID::intTime() const
{
    return 1. / m_intFrequency;
}

float PID::intFrequency() const
{
    return m_intFrequency;
}

float PID::derivTime() const
{
    return m_derivTime;
}

float PID::processValue() const
{
    return m_processValue;
}

float PID::intLimit() const
{
    return m_intLimit;
}

float PID::outputLimit() const
{
    return m_outputLimit;
}

float PID::outputValue() const
{
    return m_outputValue;
}

float PID::setpoint() const
{
    return m_setpoint;
}

float PID::setpointBandwidth() const
{
    return m_setpointBandwidth;
}

float PID::outputBandwidth() const
{
    return m_outputBandwidth;
}

void PID::configure(float pGain, float intTime, float derivTime)
{
    setPGain(pGain);
    setIntTime(intTime);
    setDerivTime(derivTime);
}

void PID::reset()
{
    m_intAcc = 0;
}

void PID::setPGain(float pGain)
{
    m_pGain = pGain;
}

void PID::setIntTime(float intTime)
{
    setIntFrequency(1. / intTime);
}

void PID::setIntFrequency(float intFrequency)
{
    m_intFrequency = intFrequency;
}

void PID::setDerivTime(float derivTime)
{
    m_derivTime = derivTime;
}

void PID::setProcessValue(float processValue)
{
    m_processValue = processValue;
}

void PID::setIntLimit(float intLimit)
{
    if (qFuzzyIsNull(intLimit)) {
        intLimit = std::numeric_limits<float>::infinity();
    }
    m_intLimit = intLimit;
}

void PID::setOutputLimit(float outputLimit)
{
    if (qFuzzyIsNull(outputLimit)) {
        outputLimit = std::numeric_limits<float>::infinity();
    }
    m_outputLimit = outputLimit;
}

void PID::setSetpoint(float setpoint)
{
    m_setpoint = setpoint;
}

void PID::setDerivativeProvider(std::function<float(float, float)> provider)
{
    if (!provider) {
        provider = Derivative<float>();
    }

    m_derivProvider = provider;
}

void PID::setSetpointBandwidth(float setpointBandwidth)
{
    m_setpointBandwidth = qFuzzyIsNull(setpointBandwidth)
            ? std::numeric_limits<float>::infinity()
            : setpointBandwidth;
    m_setpointFilter.tau = 1.0 / (2 * M_PI * m_setpointBandwidth);
}

void PID::setOutputBandwidth(float outputBandwidth)
{
    m_outputBandwidth = qFuzzyIsNull(outputBandwidth)
            ? std::numeric_limits<float>::infinity()
            : outputBandwidth;
    m_outputFilter.tau = 1.0 / (2 * M_PI * m_outputBandwidth);
}

float PID::update()
{
    return pushProcessValue(processValue());
}

float PID::pushProcessValue(float processValue, std::optional<float> dt)
{
    setProcessValue(processValue);
    updateOutput(dt);
    return outputValue();
}

void PID::storeSettings(QSettings &settings) const
{
    for (int i = 0; i < staticMetaObject.propertyCount(); ++i) {
        const auto p = staticMetaObject.property(i);
        if (!p.isWritable() || !p.isStored()) {
            continue;
        }
        settings.setValue(p.name(), p.readOnGadget(this));
    }
}

void PID::restoreSettings(QSettings &settings)
{
    for (int i = 0; i < staticMetaObject.propertyCount(); ++i) {
        const auto p = staticMetaObject.property(i);
        if (!p.isWritable() || !p.isStored()) {
            continue;
        }
        const auto v = settings.value(p.name());
        if (v.isValid()) {
            p.writeOnGadget(this, v);
        }
    }
}

void PID::updateOutput(std::optional<float> dt_opt)
{
    float dt = dt_opt.value_or(0);

    if (!m_elapsed.isValid()) {
        m_elapsed.start();
    } else {
        if (!dt_opt.has_value()) {
            dt = m_elapsed.nsecsElapsed() * 1e-9f;
        }
        m_elapsed.restart();
    }

    auto setpoint =
            (qIsFinite(m_setpointBandwidth) && !qIsNull(m_setpointBandwidth))
                ? m_setpointFilter(m_setpoint, dt)
                : m_setpoint;

    const auto err = setpoint - m_processValue;

    m_intAcc = std::clamp(m_intAcc + err * dt,
                          -m_intLimit, m_intLimit);

    auto integral = m_intAcc * m_intFrequency;
    if (debug) {
        qDebug() << "err=" << err << "intAcc=" << m_intAcc << "integral=" << integral << "m_intFreq=" << m_intFrequency;
    }

    if (!qIsFinite(integral)) {
        integral = 0;
    }

    const auto deriv = qIsNull(dt) ? 0
                                   : m_derivTime * -m_derivProvider(m_processValue, dt);

    auto outputValue = m_pGain * (err + integral + deriv);

    m_outputValue =
            (qIsFinite(m_outputBandwidth) && !qIsNull(m_outputBandwidth))
                ? m_outputFilter(outputValue, dt)
                : outputValue;

    m_outputValue = std::clamp(m_outputValue, -m_outputLimit, m_outputLimit);
}
