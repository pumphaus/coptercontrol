#include "rotorcontroller.h"
#include <QThread>
#include <QRecursiveMutex>
#include <blasterpp/blasterpp.h>

#include <QtDebug>

using namespace BlasterPP;
using namespace std::chrono;
using namespace std::chrono_literals;

namespace {
constexpr std::array<int, 4> rotorPinMap {
   18,  // front left
   27,  // front right
   22,  // rear left
   17   // rear right
};

constexpr int rotorToPin(RotorController::Rotor rotor)
{
    return rotorPinMap[rotor];
}

enum Channel
{
    UpmixedRotorChannel = 0,
};
constexpr int OutputFrequencyMultiplier = 8;

constexpr auto ThrottleMin = 1200us;
constexpr auto ThrottleMax = 2000us;
constexpr auto ThrottleOff = 1000us;

QRecursiveMutex dmaChannelMutex;

DmaChannel *dmaChannel()
{
    QMutexLocker locker(&dmaChannelMutex);
    static DmaChannel channel(13, 20000, 1us, 1, -1,
                              DmaChannel::DelayViaPwm);
    static bool setup = false;

    if (!setup) {
        for (auto pin : rotorPinMap) {
            setGpioMode(pin, ModeOutput);
            channel.setPwmPattern(UpmixedRotorChannel, OutputFrequencyMultiplier);
            channel.setPulseWidth(UpmixedRotorChannel, pin, ThrottleOff, OutputFrequencyMultiplier, DmaChannel::SetDifferential);
        }

        channel.start();
        setup = true;
    }

    return &channel;
}

constexpr nanoseconds throttleToPulseWidth(float throttle)
{
    return round<nanoseconds>(qBound(0.f, throttle, 1.f) * (ThrottleMax - ThrottleMin)) + ThrottleMin;
}

}

RotorController::RotorController(QObject *parent)
    : QObject(parent)
{
    updateOutput();
}

RotorController::~RotorController()
{
    setEnabled(false);
    usleep(100000);
    qDebug() << "Deactivated!";
}

arma::fvec4 RotorController::throttle() const
{
    return m_throttle;
}

float RotorController::throttle(RotorController::Rotor rotor) const
{
    return m_throttle[rotor];
}

bool RotorController::isEnabled() const
{
    return m_enabled;
}

void RotorController::setThrottle(const arma::fvec4 &throttle)
{
    if (all(m_throttle == throttle)) {
        return;
    }

    m_throttle = throttle;
    updateOutput();
    emit throttleChanged(m_throttle);
}

void RotorController::setThrottle(RotorController::Rotor rotor, float throttle)
{
    auto curThrottle = this->throttle();
    curThrottle[rotor] = throttle;
    setThrottle(curThrottle);
}

void RotorController::setEnabled(bool enabled)
{
    if (m_enabled == enabled) {
        return;
    }

    m_enabled = enabled;
    updateOutput();
    emit enabledChanged(m_enabled);
}

void RotorController::updateOutput()
{
    QMutexLocker locker(&dmaChannelMutex);

    for (int i = 0; i < RotorCount; ++i) {
        const auto pin = rotorToPin(Rotor(i));
        const auto pulseWidth = m_enabled ? throttleToPulseWidth(m_throttle[i])
                                          : ThrottleOff;

        dmaChannel()->setPulseWidth(UpmixedRotorChannel, pin, pulseWidth,
                                    OutputFrequencyMultiplier, DmaChannel::SetDifferential);
    }
}

int testRotors(RotorController *controller)
{
    QMutexLocker locker(&dmaChannelMutex);

    const bool wasEnabled = controller->isEnabled();
    controller->setEnabled(false);

    QThread::msleep(500);

    for (int i = 0; i < RotorController::RotorCount; ++i) {
        const auto rotor = RotorController::Rotor(i);
        const auto pin = rotorToPin(rotor);
        const auto pulseWidth = ThrottleMin;

        qDebug() << "Testing rotor" << rotor << "at pin" << pin;

        dmaChannel()->setPulseWidth(UpmixedRotorChannel, pin, pulseWidth,
                                    OutputFrequencyMultiplier, DmaChannel::SetDifferential);

        QThread::sleep(3);

        dmaChannel()->setPulseWidth(UpmixedRotorChannel, pin, ThrottleOff,
                                    OutputFrequencyMultiplier, DmaChannel::SetDifferential);

        QThread::sleep(1);
    }

    qDebug() << "Rotor test complete.";

    controller->setEnabled(wasEnabled);

    return 0;
}
