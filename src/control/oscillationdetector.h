#ifndef OSCILLATIONDETECTOR_H
#define OSCILLATIONDETECTOR_H

#include <QObject>
#include <armadillo>

class OscillationDetector : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int bufferSize READ bufferSize WRITE setBufferSize NOTIFY bufferSizeChanged)
    Q_PROPERTY(float criticalAmplitude READ criticalAmplitude WRITE setCriticalAmplitude NOTIFY criticalAmplitudeChanged)
    Q_PROPERTY(float criticalFrequency READ criticalFrequency WRITE setCriticalFrequency NOTIFY criticalFrequencyChanged)
public:
    explicit OscillationDetector(QObject *parent = nullptr);

    int bufferSize() const;
    float criticalFrequency() const;
    float criticalAmplitude() const;

public slots:
    void handleNewValue(const arma::fvec &value);
    void setBufferSize(int bufferSize);
    void setCriticalFrequency(float criticalFrequency);
    void setCriticalAmplitude(float criticalAmplitude);

signals:
    void bufferSizeChanged(int bufferSize);
    void criticalFrequencyChanged(float criticalFrequency);
    void criticalAmplitudeChanged(float criticalAmplitude);
    void oscillationsDetected(const QVector<int> &oscillatingChannels);

private:
    void pushValue(float timestamp, const arma::fvec &value);
    void checkOscillations();

    arma::fvec m_timestamps;
    arma::fmat m_buffer;
    int m_bufferPos = 0;
    float m_criticalFrequency = 0.5;
    float m_criticalAmplitude = 10;
};

#endif // OSCILLATIONDETECTOR_H
