#ifndef PID_H
#define PID_H

#include <functional>
#include <optional>

#include "elapsedtimer.h"
#include "math_util.h"

class QSettings;

class PID
{
    Q_GADGET
    Q_PROPERTY(float pGain READ pGain WRITE setPGain)
    Q_PROPERTY(float intTime READ intTime WRITE setIntTime)
    Q_PROPERTY(float intFrequency READ intFrequency WRITE setIntFrequency)
    Q_PROPERTY(float derivTime READ derivTime WRITE setDerivTime)
    Q_PROPERTY(float processValue READ processValue)
    Q_PROPERTY(float intLimit READ intLimit WRITE setIntLimit)
    Q_PROPERTY(float outputLimit READ outputLimit WRITE setOutputLimit)
    Q_PROPERTY(float outputValue READ outputValue)
    Q_PROPERTY(float setpoint READ setpoint WRITE setSetpoint STORED false)

    Q_PROPERTY(float setpointBandwidth READ setpointBandwidth WRITE setSetpointBandwidth)
    Q_PROPERTY(float outputBandwidth READ outputBandwidth WRITE setOutputBandwidth)

public:
    bool debug = false;

    explicit PID();
    PID(const PID &other);

    float pGain() const;
    float intTime() const;
    float intFrequency() const;
    float derivTime() const;
    float processValue() const;
    float intLimit() const;
    float outputLimit() const;
    float outputValue() const;
    float setpoint() const;
    float setpointBandwidth() const;
    float outputBandwidth() const;

    // debug accessor
    float intAcc() const { return m_intAcc; }

    void configure(float pGain, float intTime, float derivTime);

    void reset();

    void setPGain(float pGain);
    void setIntTime(float intTime);
    void setIntFrequency(float intFrequency);
    void setDerivTime(float derivTime);
    void setIntLimit(float intLimit);
    void setOutputLimit(float outputLimit);
    void setSetpoint(float setpoint);
    void setDerivativeProvider(std::function<float(float v, float dt)> provider);

    void setSetpointBandwidth(float setpointBandwidth);
    void setOutputBandwidth(float outputBandwidth);

    float update();
    float pushProcessValue(float processValue, std::optional<float> dt = {});

    void storeSettings(QSettings &settings) const;
    void restoreSettings(QSettings &settings);

private:
    void setProcessValue(float processValue);
    void updateOutput(std::optional<float> dt);
    void restartTimer();

    float m_pGain = 0;
    float m_intFrequency = 0;
    float m_derivTime = 0;
    float m_processValue = 0;
    int m_interval = 2;
    float m_outputLimit = std::numeric_limits<float>::infinity();
    float m_outputValue = 0;
    float m_intLimit = std::numeric_limits<float>::infinity();
    float m_intAcc = 0;
    float m_setpoint = 0;
    std::function<float(float, float)> m_derivProvider;

    float m_setpointBandwidth = std::numeric_limits<float>::infinity();
    float m_outputBandwidth = std::numeric_limits<float>::infinity();

    RCFilter<float> m_setpointFilter;
    RCFilter<float> m_outputFilter;

    ElapsedTimer m_elapsed;
};

enum PIDType {
    TypeP,
    TypePI,
    TypePID,
    TypePID_PessenIntegral,
    TypePID_SomeOvershoot,
    TypePID_NoOvershoot,
};

template<PIDType type>
constexpr std::tuple<double, double, double> zieglerNichols(double Ku, double Tu);

template<>
constexpr std::tuple<double, double, double> zieglerNichols<TypeP>(double Ku, double Tu)
{
    Q_UNUSED(Tu)
    return std::make_tuple(0.5 * Ku, std::numeric_limits<double>::infinity(), 0.0);
}

template<>
constexpr std::tuple<double, double, double> zieglerNichols<TypePI>(double Ku, double Tu)
{
    return std::make_tuple(0.45 * Ku, Tu / 1.2, 0.0);
}

template<>
constexpr std::tuple<double, double, double> zieglerNichols<TypePID>(double Ku, double Tu)
{
    return std::make_tuple(0.6 * Ku, Tu / 2.0, Tu / 8.0);
}

template<>
constexpr std::tuple<double, double, double> zieglerNichols<TypePID_PessenIntegral>(double Ku, double Tu)
{
    return std::make_tuple(0.7 * Ku, 2 * Tu / 5.0, 3 * Tu / 20.0);
}

template<>
constexpr std::tuple<double, double, double> zieglerNichols<TypePID_SomeOvershoot>(double Ku, double Tu)
{
    return std::make_tuple(Ku / 3.0, Tu / 2.0, Tu / 3.0);
}

template<>
constexpr std::tuple<double, double, double> zieglerNichols<TypePID_NoOvershoot>(double Ku, double Tu)
{
    return std::make_tuple(Ku / 5.0, Tu / 2.0, Tu / 3.0);
}

#endif // PID_H
