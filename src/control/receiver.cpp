#include "receiver.h"

#include "flightregulator.h"

#include <QCborValue>
#include <QNetworkInterface>
#include <QUdpSocket>
#include <QTimer>

using namespace Qt::StringLiterals;

static constexpr int ReceiverPort = 45450;
static constexpr int BroadcastPort = 45451;

template<class T>
T toVec(const QVariant &values)
{
    T ret;
    const auto list = values.toList();
    const auto N = std::min<int>(ret.size(), list.size());
    for (int i = 0; i < N; ++i) {
        ret[i] = list[i].toDouble();
    }
    return ret;
}

Receiver::Receiver(QObject *parent)
    : QObject(parent)
{
    qRegisterMetaType<arma::fvec2>("arma::fvec2");
    qRegisterMetaType<arma::fvec3>("arma::fvec3");

    m_socket = new QUdpSocket(this);
    connect(m_socket, &QUdpSocket::readyRead, this, &Receiver::readDatagrams);
    if (!m_socket->bind(QHostAddress::AnyIPv4, ReceiverPort)) {
        qFatal("Failed to bind reciever socket!");
    }

    auto *broadcastTimer = new QTimer(this);
    connect(broadcastTimer, &QTimer::timeout, this, &Receiver::broadcastHeartbeat);
    broadcastTimer->start(500);
}

arma::fvec3 Receiver::angularRate() const
{
    return m_angularRate;
}

arma::fvec2 Receiver::angles() const
{
    return m_angles;
}

float Receiver::climbRate() const
{
    return m_climbRate;
}

bool Receiver::isRegulatorEnabled() const
{
    return m_regulatorEnabled;
}

void Receiver::attachToRegulator(FlightRegulator *regulator)
{
    connect(this, &Receiver::climbRateChanged,
            regulator, &FlightRegulator::setClimbRateSetpoint, Qt::UniqueConnection);
    connect(this, &Receiver::regulatorEnabledChanged,
            regulator, &FlightRegulator::setActive, Qt::UniqueConnection);
    connect(this, &Receiver::anglesChanged,
            regulator, &FlightRegulator::setAngleSetpoint, Qt::UniqueConnection);
    connect(this, &Receiver::angularRateChanged,
            regulator, &FlightRegulator::setAngularRateSetpoint, Qt::UniqueConnection);
}

void Receiver::broadcastHeartbeat()
{
    const auto msg = "COPTERCONTROL"_ba;
    const auto written =
        m_socket->writeDatagram(msg, QHostAddress::Broadcast, BroadcastPort);
    if (written != msg.size()) {
        qDebug() << "Failed to write broadcast";
    }
}

void Receiver::readDatagrams()
{
    while (m_socket->hasPendingDatagrams()) {
        QByteArray data(m_socket->pendingDatagramSize(), Qt::Uninitialized);
        m_socket->readDatagram(data.data(), data.size());
        processDatagram(data);
    }
}

void Receiver::processDatagram(const QByteArray &data)
{
    QCborValue value = QCborValue::fromCbor(data);
    if (value.isNull()) {
        return;
    }
    QVariantMap map = value.toVariant().toMap();
    const auto timestamp = map["t"].toUInt();
    if (timestamp < m_lastTimeStamp) {
        return;
    }
    m_lastTimeStamp = timestamp;

    m_angles = toVec<arma::fvec2>(map["a"]);
    m_angularRate = toVec<arma::fvec3>(map["ar"]);
    m_climbRate = map["cr"].toFloat();

    const bool nowEnabled = map["ena"].toBool();
    if (m_regulatorEnabled != nowEnabled) {
        m_regulatorEnabled = nowEnabled;
        emit regulatorEnabledChanged(m_regulatorEnabled);
    }

    emit anglesChanged(m_angles);
    emit angularRateChanged(m_angularRate);
    emit climbRateChanged(m_climbRate);
}
