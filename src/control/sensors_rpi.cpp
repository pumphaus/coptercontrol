#include "sensors.h"

#include "elapsedtimer.h"
#include <QEventLoop>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSettings>
#include <QTimer>
#include <QTimerEvent>
#include <QtDebug>

#include <optional>

#include "MPU6050.h"
#include "HMC5883L.h"
#include "MS561101BA.h"
#include "rcw0001.h"
#include "blasterpp/blasterpp.h"
#include "math_util.h"
#include "voltagemonitor.h"

constexpr int FullscaleAccelRange = MPU6050_ACCEL_FS_2;
constexpr int FullscaleGyroRange = MPU6050_GYRO_FS_2000;
constexpr float MpuBandwidth = 98.0;
constexpr auto MpuDLPF = MPU6050_DLPF_BW_98;
constexpr auto MpuClockDivider = 1;  // 1 kHz / (1 + rate) = 1 kHz / 2 = 500 Hz output rate
constexpr float MpuOutputRate = 500.0f;
constexpr float MpuOutputPeriod = 1.0f / MpuOutputRate;
constexpr int MpuInterruptPin = 23;

constexpr float p0 = 101325.f;
constexpr float rho0 = 1.2041f;
constexpr float g = 9.81f;

#define repeat(n) for (int __i = 0; __i < n; ++__i)

enum AngleIndex {
    YawIndex = 2,
    PitchIndex = 1,
    RollIndex = 0,
};

template<class T>
arma::fvec3 mapAcceleration(T ax, T ay, T az)
{
    constexpr auto LShift = 14 - FullscaleAccelRange;
    constexpr float LsbPerG = 1 << LShift;

    return arma::fvec3{float(ax), float(ay), float(az)} / LsbPerG;
}

template<class T>
arma::fvec3 mapGyro(T ax, T ay, T az)
{
    constexpr float Div = 131.0f / (1 << FullscaleGyroRange);

    return arma::fvec3{float(ax), float(ay), float(az)} / Div;
}

arma::fmat33 Rx(float phi)
{
    using std::cos;
    using std::sin;

    return arma::fmat33{
        {1,         0,        0},
        {0,  cos(phi), sin(phi)},
        {0, -sin(phi), cos(phi)}
    };
}
arma::fmat33 Ry(float theta)
{
    using std::cos;
    using std::sin;

    return arma::fmat33{
        {cos(theta), 0, -sin(theta)},
        {         0, 1,           0},
        {sin(theta), 0,  cos(theta)}
    };
}
arma::fmat33 Rz(float psi)
{
    using std::cos;
    using std::sin;

    return arma::fmat33{
        { cos(psi), sin(psi), 0},
        {-sin(psi), cos(psi), 0},
        {        0,        0, 1}
    };
}

constexpr float pressureToHeight(float p)
{
    return p0 / (rho0 * g) * std::log(p0 / p);
}

struct MS5611Sampler
{
    bool sampleTemperatureNext = true;
    ElapsedTimer timer;
    const int maxSampTime_ns;
    MS561101BA * const baro;

    MS5611Sampler(int maxSampTime_us, MS561101BA *baro)
        : maxSampTime_ns(maxSampTime_us * 1000), baro(baro)
    {
        baro->initPressureConversion();
        timer.start();
        sampleTemperatureNext = true;

    }

    std::optional<BarometerReading> operator()()
    {
        if (timer.nsecsElapsed() < maxSampTime_ns) {
            return {};
        }
        bool fetchedPressure = false;
        if (sampleTemperatureNext) {
            fetchedPressure = true;
            baro->fetchPressure();
            baro->initTemperatureConversion();
        } else {
            baro->fetchTemperature();
            baro->initPressureConversion();
        }
        sampleTemperatureNext = !sampleTemperatureNext;
        timer.restart();

        float pressure, temperature;
        if (fetchedPressure && baro->readValues(&pressure, &temperature)) {
            return BarometerReading{pressure, temperature};
        }
        return {};
    }
};

// Calculates the climb rate over FilterNSamps samples of the ultrasonic sensor.
// The time constant is therefore FilterNSamps * ultrasonicCycleTime.
struct ClimbRateCalculator
{
    static constexpr float slow_fc = 0.5;  // Hz
    static constexpr float slow_tau = 1.0 / (2 * M_PI * slow_fc);
    static constexpr float fast_fc = 4; // Hz
    static constexpr float fast_tau = 1.0 / (2 * M_PI * fast_fc);

    mutable ElapsedTimer warmupTimer;

    struct {
        IIRFilter<float> slowHighpass =
                std::make_from_tuple<IIRFilter<float>>(
                        butter(2, slow_fc / MpuOutputRate, TypeHighpass_DeltaInput));
        IIRFilter<float> fastHighpass =
                std::make_from_tuple<IIRFilter<float>>(
                        butter(2, fast_fc / MpuOutputRate, TypeHighpass_DeltaInput));
        float slowRate = 0;
        float fastRate = 0;
        float accel = 0;
    } zAccelCalc;

    struct {
        TimedButterworthFilter<float> smooth { 2, slow_tau };
        TimedDerivative<float> deriv;
        float rate = 0;
        float h = 0;
    } barometric;

    struct {
        TimedDerivative<float> deriv;
        IQRFilter<float> outlierRemover { 10 };
        TimedButterworthFilter<float> smooth { 2, fast_tau };
        float rate = 0;
        float height = 0;
    } ultrasonic;

    bool isWarmedUp() const {
        return warmupTimer.isValid()
                && warmupTimer.hasExpired(slow_tau * 8 * 1000);
    }

    void pushZAccel(float az)
    {
        const auto dv = az * MpuOutputPeriod;
        zAccelCalc.fastRate = zAccelCalc.fastHighpass(dv);
        zAccelCalc.slowRate = zAccelCalc.slowHighpass(dv);
        zAccelCalc.accel = az;
    }

    void pushBarometricHeight(float height)
    {
        const auto smooth_h = barometric.smooth(height);
        barometric.h = height;
        barometric.rate = barometric.deriv(smooth_h);
    }

    void pushUltrasonicHeight(float height)
    {
        ultrasonic.rate = ultrasonic.smooth(ultrasonic.outlierRemover(ultrasonic.deriv(height)));
        ultrasonic.height = height;
    }

    void setDebugInfo(QObject *object)
    {
        object->setProperty("baroRate", barometric.rate);
        object->setProperty("baroHeight", barometric.h);
        object->setProperty("zFastVeloc", zAccelCalc.fastRate);
        object->setProperty("zSlowVeloc", zAccelCalc.slowRate);
        object->setProperty("zAccel", zAccelCalc.accel);
        object->setProperty("ultrasonicHeight", ultrasonic.height);
        object->setProperty("ultrasonicRate", ultrasonic.rate);
    }

    float climbRate() const {
        if (!warmupTimer.isValid()) {
            warmupTimer.start();
        }
        if (!isWarmedUp()) {
            return 0;
        }
        if (std::isfinite(ultrasonic.rate)) {
            return ultrasonic.rate + zAccelCalc.fastRate;
        } else {
            return barometric.rate + zAccelCalc.slowRate;
        }
    }
};

arma::fvec3 offsetForTemperature(const QVariant &offsetSpec, float temp)
{
    using namespace arma;

    if (offsetSpec.userType() == qMetaTypeId<fvec3>()) {
        return offsetSpec.value<fvec3>();
    } else if (offsetSpec.userType() == qMetaTypeId<fvec>()) {
        return offsetSpec.value<fvec>();
    } else if (offsetSpec.userType() == qMetaTypeId<fmat>()) {
        const fmat p = offsetSpec.value<fmat>();
        fvec out(p.n_rows);
        for (uint i = 0; i < p.n_rows; ++i) {
            out[i] = fvec{polyval(p.row(i), fvec{temp})}[0];
        }
        return out;
    } else if (offsetSpec.canConvert<float>()) {
        return fvec3(fill::ones) * offsetSpec.value<float>();
    }
    return fvec3(fill::zeros);
}

Sensors::Sensors(QObject *parent)
    : QObject(parent)
{
    qRegisterMetaType<arma::fvec>("arma::fvec");
    qRegisterMetaType<arma::fvec3>("arma::fvec3");
    qRegisterMetaType<arma::fmat33>("arma::fmat33");
    qRegisterMetaType<arma::fmat>("arma::fmat");

    m_accelgyro = std::make_unique<MPU6050>();
    if (!m_accelgyro->testConnection()) {
        qFatal("Failed to connect to the accelerometer and gyroscope!");
    }

    // We have to do this twice, because... reasons? The interrupt won't work
    // correctly otherwise.
    repeat(2) {
        m_accelgyro->setI2CBypassEnabled(true);
        m_accelgyro->setI2CMasterModeEnabled(false);
        m_accelgyro->setClockSource(MPU6050_CLOCK_PLL_ZGYRO);
        m_accelgyro->setFullScaleGyroRange(FullscaleGyroRange);
        m_accelgyro->setFullScaleAccelRange(FullscaleAccelRange);
        m_accelgyro->setDLPFMode(MpuDLPF);
        m_accelgyro->setRate(MpuClockDivider);
        m_accelgyro->setIntDataReadyEnabled(true);
        m_accelgyro->setSleepEnabled(false);
    }

    m_mag = std::make_unique<HMC5883L>();
    if (!m_mag->testConnection()) {
        qFatal("Failed to connect to magnetometer!");
    }
    m_mag->initialize();

    m_baro = std::make_unique<MS561101BA>();
    if (!m_baro->testConnection()) {
        qFatal("Failed to connect to barometer!");
    }
    m_baro->initialize();
    m_baro->setOverSampleRate(MS561101BA_OSR_4096);
    m_baroSampler = MS5611Sampler(MS561101BA_MAX_CONVERSION_TIME_OSR_4096,
                                  m_baro.get());

    m_dist = new Rcw0001(this);
    m_dist->setAutoProcessSamples(false);  // No auto-processing; everything's triggered by the MPU6050

    m_climbRateCalc = std::make_unique<ClimbRateCalculator>();

    connect(m_dist, &Rcw0001::distanceChanged, this, &Sensors::updateAltitude);

    // 5 samples at 30 ms each are 150 ms. Use the same time constant for both the
    // median filter of the relative altitude smoother and the absolute altitude
    // smoother.
    m_relativeAltitudeCalc = MedianFilter<float>(5);
    m_absoluteAltitudeCalc = RCFilter<float>(5 * m_dist->cycleTime());

    // Filter the angular velocity output with a 2nd order Butterworth
    m_angularVelocityFilter = std::make_from_tuple<IIRFilter<float, 3>>(butter(2, MpuBandwidth / MpuOutputRate));

    m_voltMon = new VoltageMonitor(this);
    connect(m_voltMon, &VoltageMonitor::voltageChanged, this, &Sensors::voltageChanged);

    // Poll the interrupt status register instead of watching the actual pin.
    // The interrupt pulse is only 50 µs long and can easily be missed.
    m_interruptPollTimer.start(0, Qt::PreciseTimer, this);

    // Debug dt
    connect(this, &Sensors::updateComplete, this,
            [this, elapsed=ElapsedTimer(), i=0, dt=arma::running_stat<double>()]() mutable
    {
        if (!property("debug").toBool()) {
            return;
        }

        if (!elapsed.isValid()) {
            elapsed.start();
        }
        const float curDt = elapsed.nsecsElapsed() * 1e-6;
        dt(curDt);
        elapsed.restart();
        ++i;
        if (i >= 100) {
            i = 0;
            qDebug() << "Mean dt:" << dt.mean();
            dt.reset();
        }
    });
}

Sensors::~Sensors()
{
    m_accelgyro->reset();
    usleep(100e3);  // Sleep for 100 ms
}

arma::fvec3 Sensors::acceleration() const
{
    return m_acceleration;
}

float Sensors::pitch() const
{
    return m_angles[PitchIndex];
}

float Sensors::roll() const
{
    return m_angles[RollIndex];
}

float Sensors::yaw() const
{
    return m_angles[YawIndex];
}

arma::fmat33 Sensors::rotationMatrix() const
{
    return m_rotationMatrix;
}

arma::fvec3 Sensors::currentAccelerationOffset() const
{
    return offsetForTemperature(m_accelerationOffset, m_imuTemperature);
}

arma::fvec3 Sensors::currentAngularVelocityOffset() const
{
    return offsetForTemperature(m_angularVelocityOffset, m_imuTemperature);
}

QVariant Sensors::accelerationOffset() const
{
    return m_accelerationOffset;
}

QVariant Sensors::angularVelocityOffset() const
{
    return m_angularVelocityOffset;
}

arma::fvec3 Sensors::headingOffset() const
{
    return m_headingOffset;
}

arma::fvec3 Sensors::headingScale() const
{
    return m_headingScale;
}

float Sensors::relativeAltitude() const
{
    return m_relativeAltitude;
}

float Sensors::climbRate() const
{
    return m_climbRate;
}

float Sensors::climbAcceleration() const
{
    return m_climbAcceleration;
}

float Sensors::voltage() const
{
    return m_voltMon->voltage();
}

float Sensors::pressure() const
{
    return m_pressure;
}

float Sensors::temperature() const
{
    return m_temperature;
}

float Sensors::imuTemperature() const
{
    return m_imuTemperature;
}

float Sensors::absoluteAltitude() const
{
    return m_absoluteAltitude;
}

arma::fvec3 Sensors::angularVelocity() const
{
    return m_angularVelocity;
}

arma::fvec3 Sensors::uncalibratedAcceleration() const
{
    return m_uncalibratedAcceleration;
}

arma::fvec3 Sensors::uncalibratedAngularVelocity() const
{
    return m_uncalibratedAngularVelocity;
}

arma::fvec3 Sensors::heading() const
{
    return m_heading;
}

arma::fvec3 Sensors::uncalibratedHeading() const
{
    return m_uncalibratedHeading;
}

arma::fvec3 Sensors::angles() const
{
    return m_angles;
}

void Sensors::update()
{
    // Read sensors, map values
    {
        qint16 ax, ay, az, gx, gy, gz;
        m_accelgyro->getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

        const qint16 temp = m_accelgyro->getTemperature();
        // MPU 6050 Register Specs, page 30, section 4.18 "Temperature Measurement"
        // Temperature in degrees C = (TEMP_OUT Register Value as a signed quantity)/340 + 36.53
        m_imuTemperature = temp / 340.0f + 36.53f;
        emit imuTemperatureChanged(m_imuTemperature);

        // Adjust for the rotation of the chip on the PCB (rotated 90° around the Z axis)
        m_uncalibratedAcceleration = mapAcceleration<qint16>(-ay, ax, az);
        m_uncalibratedAngularVelocity = mapGyro<qint16>(-gy, gx, gz);

        m_acceleration = m_uncalibratedAcceleration - currentAccelerationOffset();
        m_angularVelocity = m_uncalibratedAngularVelocity - currentAngularVelocityOffset();

        // Filter the angular velocity again, because the internal low-pass isn't that good...
        m_angularVelocity = m_angularVelocityFilter(m_angularVelocity);

        emit uncalibratedAccelerationChanged(m_uncalibratedAcceleration);
        emit uncalibratedAngularVelocityChanged(m_uncalibratedAngularVelocity);

        emit accelerationChanged(m_acceleration);
        emit angularVelocityChanged(m_angularVelocity);

        if (m_mag->getReadyStatus()) {
            qint16 mx, my, mz;
            m_mag->getHeading(&mx, &my, &mz);

            // Adjust for the rotation of the chip on the PCB (rotated 90° around the Z axis)
            m_uncalibratedHeading = { float(-my), float(mx), float(mz) };
            m_heading = (m_uncalibratedHeading - m_headingOffset) / m_headingScale;

            emit uncalibratedHeadingChanged(m_uncalibratedHeading);
            emit headingChanged(m_heading);
        }
    }

    // Shorthand
    const float ax = m_acceleration[0];
    const float ay = m_acceleration[1];
    const float az = m_acceleration[2];

    // Gimbal lock mitigation factor
    static constexpr float mu = 0.01f;

    // This is valid for R_xyz, (= R_x * R_y * R_z).
    // First rotation about z-axis (R_z, yaw), then y-axis (R_y, pitch) and finally
    // about the the x-axis (R_x, roll).
    const arma::fvec2 anglesFromAccel = arma::fvec2 {
        // roll
        std::atan2(ay, std::copysign(std::sqrt(az * az + mu * ax * ax), az)),
        // pitch
        std::atan2(-ax, std::sqrt(ay * ay + az * az)),
    } * (180.0 / M_PI);

    constexpr float dt = 1.0 / MpuOutputRate;
    constexpr float tau = 1.f; // timeconstant for the complementary filter (seconds)

    // On first measurement, use only the non-integrated data
    const float alpha = arma::all(m_angles == 0) ? 1.0f
                                                 : dt / (tau + dt);

    // Fuse accel and gyro for roll and pitch
    m_angles[RollIndex] = (1 - alpha) * (m_angles[RollIndex] + m_angularVelocity[RollIndex] * dt) + alpha * anglesFromAccel[RollIndex];
    m_angles[PitchIndex] = (1 - alpha) * (m_angles[PitchIndex] + m_angularVelocity[PitchIndex] * dt) + alpha * anglesFromAccel[PitchIndex];

    // Compensate the heading for the tilt
    const arma::fvec3 compensatedHeading = Ry(m_angles[PitchIndex] * M_PI/180).t() * Rx(m_angles[RollIndex] * M_PI/180).t() * m_heading;
    const float magYaw = std::atan2(compensatedHeading[0], compensatedHeading[1]) * 180 / M_PI;

    // Fuse magnetometer and gyro
    m_angles[YawIndex] = (1 - alpha) * (m_angles[YawIndex] + m_angularVelocity[YawIndex] * dt) + alpha * magYaw;

    emit anglesChanged(m_angles);
    emit rollChanged(roll());
    emit pitchChanged(pitch());
    emit yawChanged(yaw());

    m_rotationMatrix = Rx(m_angles[RollIndex] * M_PI/180)
            * Ry(m_angles[PitchIndex] * M_PI/180)
            * Rz(m_angles[YawIndex] * M_PI/180);

    emit rotationMatrixChanged(rotationMatrix());

    // Update the barometer
    if (auto reading = m_baroSampler()) {
        m_pressure = reading->pressure;
        m_temperature = reading->temperature;
        const auto h = pressureToHeight(m_pressure);
        m_absoluteAltitude = m_absoluteAltitudeCalc(h);

        setProperty("rawAbsoluteHeight", h);

        // Ensure that this happens before the ultrasonic distance sensor update!
        // We want to synchronize everything to the ultrasonic sensor in updateAltitude().
        m_climbRateCalc->pushBarometricHeight(h);

        emit pressureChanged(m_pressure);
        emit temperatureChanged(m_temperature);
        emit absoluteAltitudeChanged(m_absoluteAltitude);
    }

    // Update distance sensor
    m_dist->processSamples();

    const auto inertial_a = (rotationMatrix().t() * m_acceleration).eval();
    m_climbAcceleration = (inertial_a[2] - 1) * -9.81f;
    emit climbAccelerationChanged(m_climbAcceleration);

    m_climbRateCalc->pushZAccel(m_climbAcceleration);
    m_climbRate = m_climbRateCalc->climbRate();
    emit climbRateChanged(m_climbRate);

    m_climbRateCalc->setDebugInfo(this);

    // Signal that this round of updates is complete
    emit updateComplete();

    // Update voltage monitor (this is rate limited and sort of async)
    m_voltMon->initiateUpdate();
}

void Sensors::setAccelerationOffset(const QVariant &accelerationOffset)
{
    if (m_accelerationOffset == accelerationOffset)
        return;

    m_accelerationOffset = accelerationOffset;
    emit accelerationOffsetChanged(m_accelerationOffset);
}

void Sensors::setAngularVelocityOffset(const QVariant &angularVelocityOffset)
{
    if (m_angularVelocityOffset == angularVelocityOffset)
        return;

    m_angularVelocityOffset = angularVelocityOffset;
    emit angularVelocityOffsetChanged(m_angularVelocityOffset);
}

void Sensors::setHeadingOffset(const arma::fvec3 &headingOffset)
{
    if (all(m_headingOffset == headingOffset)) {
        return;
    }

    m_headingOffset = headingOffset;
    emit headingOffsetChanged(m_headingOffset);
}

void Sensors::setHeadingScale(const arma::fvec3 &headingScale)
{
    if (all(m_headingScale == headingScale)) {
        return;
    }

    m_headingScale = headingScale;
    emit headingScaleChanged(m_headingScale);
}

void Sensors::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == m_interruptPollTimer.timerId()
            && m_accelgyro->getIntDataReadyStatus())
    {
        update();
    }
}

void Sensors::updateAltitude()
{
    setProperty("rawRelativeHeight", m_dist->distance());
    m_relativeAltitude = m_relativeAltitudeCalc(m_dist->distance());
    m_climbRateCalc->pushUltrasonicHeight(m_dist->distance());
    emit relativeAltitudeChanged(m_relativeAltitude);
}

/*********************************************
 *             Calibration routines          *
 *********************************************/

int calibrateTemperatureDependency(QSettings &settings, Sensors *sensors)
{
    using namespace std::chrono;
    constexpr auto MaxTime_ms = duration_cast<milliseconds>(minutes(3));
    constexpr float MinTempDiff_degC = 7;

    using namespace arma;
    QVector<float> accelValues;
    QVector<float> gyroValues;
    QVector<float> temp;

    QEventLoop loop;
    ElapsedTimer elapsed;

    QObject::connect(sensors, &Sensors::updateComplete, &loop, [&]() {
        const auto a = sensors->uncalibratedAcceleration();
        accelValues << a[0] << a[1] << a[2];
        const auto g = sensors->uncalibratedAngularVelocity();
        gyroValues << g[0] << g[1] << g[2];
        temp << sensors->imuTemperature();
    });

    qDebug() << "Calibrating accelerometer and gyro. Please keep the quadcopter still and on an even surface!";

    QTimer timer;
    timer.setInterval(100);
    QObject::connect(&timer, &QTimer::timeout, &loop, [&]() {
        printf("\rTemp 0: %.2f, cur: %.2f      ", temp.front(), temp.back());
        fflush(stdout);
        if (std::abs(temp.back() - temp.front()) >= MinTempDiff_degC
                || elapsed.elapsed() > MaxTime_ms.count())
        {
            loop.quit();
        }
    });
    timer.start();

    elapsed.start();
    int ret = loop.exec();
    if (ret != 0) {
        return ret;
    }

    const fvec3 Gravity { 0, 0, 1 };

    const fmat accelMat = (fmat(accelValues.data(), 3, accelValues.size() / 3).each_col() - Gravity).t();
    const fmat gyroMat = fmat(gyroValues.data(), 3, gyroValues.size() / 3).t();

    const fvec tempV = fvec(temp.data(), temp.size());

    fmat accelCalib(3, 2);
    fmat gyroCalib(3, 2);

    qDebug() << "temp 0:" << tempV[0] << "temp last:" << tempV[tempV.size() - 1];
    qDebug() << "accelMat size:" << accelMat.n_rows << "x" << accelMat.n_cols;
    qDebug() << "gyroMat size:" << gyroMat.n_rows << "x" << gyroMat.n_cols;
    qDebug() << "temp size:" << tempV.size();

    for (uint i = 0; i < accelCalib.n_rows; ++i) {
        accelCalib(i, span::all) = polyfit(tempV, accelMat(span::all, i), 1).t();
        gyroCalib(i, span::all) = polyfit(tempV, gyroMat(span::all, i), 1).t();
    }

    qDebug() << "Accel calib:" << accelCalib;
    qDebug() << "Gyro calib:" << gyroCalib;

    settings.setValue("calibration/accelOffset", QVariant::fromValue(accelCalib));
    settings.setValue("calibration/gyroOffset", QVariant::fromValue(gyroCalib));

    return 0;
}

int calibrateMagnetometer(QSettings &settings, Sensors *sensors)
{
    using namespace arma;
    QVector<fvec3> headings;

    qDebug() << "Calibrating magnetometer. Please move and rotate the quadcopter in a figure 8 way!";

    constexpr double T_calib = 10000;  // 10 seconds calibration time

    QEventLoop loop;
    QTimer timer;
    timer.setInterval(100);
    QObject::connect(&timer, &QTimer::timeout, sensors, [&]() {
        headings.append(sensors->uncalibratedHeading());

        static int nFired = 0;
        ++nFired;
        std::cerr << "." << std::flush;
        if (timer.interval() * nFired >= T_calib) {
            std::cerr << std::endl;
            loop.quit();
        }
    });
    timer.start();

    int ret = loop.exec();
    if (ret != 0) {
        return ret;
    }

    fmat headingMat(headings.size(), 3);
    for (int row = 0; row < headingMat.n_rows; ++row) {
        headingMat.row(row) = headings.at(row).t();
    }
    auto result = ellipsoidFit(conv_to<mat>::from(headingMat));
    settings.setValue("calibration/headingOffset", QVariant::fromValue(result.center));
    settings.setValue("calibration/headingScale", QVariant::fromValue(result.radii));

    return 0;
}

void loadCalibration(QSettings &settings, Sensors *sensors)
{
    using namespace arma;

    const auto accelOffset =   settings.value("calibration/accelOffset");
    const auto gyroOffset =    settings.value("calibration/gyroOffset");
    const auto headingOffset = settings.value("calibration/headingOffset");
    const auto headingScale =  settings.value("calibration/headingScale");

    if (accelOffset.canConvert<fmat>()
        || accelOffset.canConvert<fvec>())
    {
        sensors->setAccelerationOffset(accelOffset);
    } else {
        qWarning() << "Failed to load accel offset";
    }

    if (gyroOffset.canConvert<fmat>()
        || gyroOffset.canConvert<fvec>())
    {
        sensors->setAngularVelocityOffset(gyroOffset);
    } else {
        qWarning() << "Failed to load gyro offset";
    }

    if (headingOffset.canConvert<fvec>()) {
        sensors->setHeadingOffset(headingOffset.value<fvec>());
    } else {
        qWarning() << "Failed to load heading offset";
    }

    if (headingScale.canConvert<fvec>()) {
        sensors->setHeadingScale(headingScale.value<fvec>());
    } else {
        qWarning() << "Failed to load heading scale";
    }

    qInfo() << "Loaded calibration from" << settings.fileName();
}
