#ifndef TUNINGINTERFACE_H
#define TUNINGINTERFACE_H

#include <QObject>
#include <QMap>
#include <QUrl>
#include <QVariant>
#include <QVector>

class FlightRegulator;
class Sensors;
class QTimer;
class QWebChannel;

class TuningInterface : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantMap pidParams READ pidParams NOTIFY pidParamsChanged)
public:
    struct Sample
    {
        qint64 timestamp;
        QVariant value;
    };

    explicit TuningInterface(QObject *parent = nullptr);
    ~TuningInterface();

    void setSensors(Sensors *sensors);
    Sensors *sensors() const { return m_sensors; }

    void setFlightRegulator(FlightRegulator *regulator);
    FlightRegulator *flightRegulator() const { return m_regulator; }

    QVariantMap pidParams() const;
    Q_INVOKABLE bool setPidParam(const QString &pidName, int subIndex, const QByteArray &paramName, const QVariant &value);

signals:
    void propertySamplesReady(const QJsonObject &samples);
    void pidParamsChanged();

public slots:

private:
    void stashPropertySample(const QByteArray &name, const Sample &sample);
    void sendUpdates();

    FlightRegulator *m_regulator = nullptr;
    Sensors *m_sensors = nullptr;

    QMap<QByteArray, QVector<Sample>> m_cache;
    QTimer *m_broadcastTimer = nullptr;

    QUrl m_influxUrl;
};

Q_DECLARE_METATYPE(TuningInterface::Sample)

QWebChannel *createWebChannel(QObject *parent);

#endif // TUNINGINTERFACE_H
