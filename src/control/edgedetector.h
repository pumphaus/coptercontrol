#ifndef EDGEDETECTOR_H
#define EDGEDETECTOR_H

#include <QBasicTimer>
#include <QObject>
#include <QVector>

class EdgeDetector : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVector<int> pins READ pins WRITE setPins NOTIFY pinsChanged)
public:
    explicit EdgeDetector(QObject *parent = nullptr);
    explicit EdgeDetector(const QVector<int> &pins, QObject *parent = nullptr);

    QVector<int> pins() const;

public slots:
    void setPins(const QVector<int> &pins);

signals:
    void risingEdge(int pin);
    void fallingEdge(int pin);
    void pinsChanged(const QVector<int> &pins);

protected:
    void timerEvent(QTimerEvent *event) override;

private:
    void checkEdges();

    QBasicTimer m_pollTimer;
    QVector<int> m_pins;
    quint32 m_lastMask = 0;
};

#endif // EDGEDETECTOR_H
