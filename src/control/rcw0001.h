#ifndef RCW0001_H
#define RCW0001_H

#include <QObject>
#include <QBasicTimer>

class Rcw0001 : public QObject
{
    Q_OBJECT
    Q_PROPERTY(float distance READ distance NOTIFY distanceChanged)
public:
    explicit Rcw0001(QObject *parent = nullptr);
    float distance() const;

    void setAutoProcessSamples(bool autoProcess);
    bool isAutoProcessingSamples() const;

    float cycleTime() const;

signals:
    void distanceChanged(float distance);

public slots:
    void processSamples();
    
protected:
    void timerEvent(QTimerEvent *event) override;

private:
    void updateDistance(int sampleDistance);
    void signalInvalidMeasurement();
    
    QBasicTimer m_readoutTimer;
    float m_distance = 0;
    int m_lastPos = 0;
    int m_lastRisingEdgeId = -1;
};

#endif // RCW0001_H
