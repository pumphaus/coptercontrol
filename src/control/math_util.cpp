#include "math_util.h"

// Based on https://github.com/aleksandrbazhin/ellipsoid_fit_python, which is
// in turn based on http://www.mathworks.com/matlabcentral/fileexchange/24693-ellipsoid-fit
EllipsoidFitResult ellipsoidFit(const arma::mat &X)
{
    using namespace arma;

    const rowvec x = X(span::all, 0).t();
    const rowvec y = X(span::all, 1).t();
    const rowvec z = X(span::all, 2).t();

    mat D(9, X.n_rows);
    D(0, span::all) = x % x + y % y - 2 * z % z;
    D(1, span::all) = x % x + z % z - 2 * y % y;
    D(2, span::all) = 2 * x % y;
    D(3, span::all) = 2 * x % z;
    D(4, span::all) = 2 * y % z;
    D(5, span::all) = 2 * x;
    D(6, span::all) = 2 * y;
    D(7, span::all) = 2 * z;
    D(8, span::all) = ones(x.n_elem).t();

    const vec d2 = (x % x + y % y + z % z).t();  // rhs for LLSQ

    const vec u = solve(D * D.t(), D * d2);

    vec v(10);
    v(0) = u(0) + 1 * u(1) - 1;
    v(1) = u(0) - 2 * u(1) - 1;
    v(2) = u(1) - 2 * u(0) - 1;
    v(span(3, v.n_elem - 1)) = u(span(2, u.n_elem - 1));

    const mat A {
        { v(0), v(3), v(4), v(6) },
        { v(3), v(1), v(5), v(7) },
        { v(4), v(5), v(2), v(8) },
        { v(6), v(7), v(8), v(9) }
    };

    const vec center = solve(-A(span(0, 2), span(0, 2)), v(span(6, 8)));

    mat translation_matrix = eye(4, 4);
    translation_matrix(3, span(0, 2)) = center.t();

    const mat R = (translation_matrix * A) * translation_matrix.t();

    mat evecs;
    vec evals;
    eig_sym(evals, evecs, R(span(0, 2), span(0, 2)) / -R(3, 3));

    vec radii = sqrt(1. / abs(evals));
    radii %= sign(evals);

    EllipsoidFitResult result {
        std::move(center),
                std::move(radii),
                std::move(evecs)
    };

    return result;
}
