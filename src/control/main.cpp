#include <QCoreApplication>
#include "elapsedtimer.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QEventLoop>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QSettings>
#include <QThread>
#include <QTimer>
#include <QtDebug>
#include <QWebChannel>

#include <signal.h>

#include <cmath>
#include "I2Cdev.h"
#include "flightregulator.h"
#include "sensors.h"
#include "math_util.h"
#include "oscillationdetector.h"
#include "rtthread.h"
#include "tuninginterface.h"
#include "rotorcontroller.h"
#include "receiver.h"

void terminate(int signal)
{
    qApp->quit();
}

void registerMetaTypes()
{
    qRegisterMetaType<arma::vec>();
    qRegisterMetaType<arma::fvec>();
    qRegisterMetaType<arma::rowvec>();
    qRegisterMetaType<arma::frowvec>();
    qRegisterMetaType<arma::mat>();
    qRegisterMetaType<arma::fmat>();

    QMetaType::registerConverter<arma::vec, arma::fvec>([](const arma::vec &v) {
        return arma::conv_to<arma::fvec>::from(v);
    });
    QMetaType::registerConverter<arma::fvec, arma::vec>([](const arma::fvec &v) {
        return arma::conv_to<arma::vec>::from(v);
    });
    QMetaType::registerConverter<arma::mat, arma::fmat>([](const arma::mat &v) {
        return arma::conv_to<arma::fmat>::from(v);
    });
    QMetaType::registerConverter<arma::fmat, arma::mat>([](const arma::fmat &v) {
        return arma::conv_to<arma::mat>::from(v);
    });
}

int main(int argc, char **argv)
{
    if (   signal(SIGINT, &terminate) == SIG_ERR
            || signal(SIGTERM, &terminate) == SIG_ERR
            || signal(SIGABRT, &terminate) == SIG_ERR
            || signal(SIGBUS, &terminate) == SIG_ERR
            || signal(SIGQUIT, &terminate) == SIG_ERR)
    {
        qFatal("An error occurred while setting a signal handler.");
        return EXIT_FAILURE;
    }

    registerMetaTypes();

    I2Cdev::initialize("/dev/i2c-1");
    QCoreApplication app(argc, argv);

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption({"calibrate-accelgyro", "Perform accelerometer and gyro calibration"});
    parser.addOption({"calibrate-magnetometer", "Perform magnetometer calibration"});
    parser.addOption({"test-rotors", "Perform rotor test sequence"});
    parser.addOption({"const-throttle", "Apply constant throttle to all rotors", "throttle"});

    parser.process(app);

    RTThread rtThread;

    QObject *rtRootObject = new QObject;
    Sensors *sensors = new Sensors(rtRootObject);
    RotorController *rotorController = new RotorController(rtRootObject);
    FlightRegulator *regulator = new FlightRegulator(rtRootObject);
    regulator->setSensors(sensors);
    regulator->setRotorController(rotorController);

    rtRootObject->moveToThread(&rtThread);
    QObject::connect(&rtThread, &RTThread::finished, rtRootObject, &QObject::deleteLater);

    rtThread.start();

    QSettings settings("coptercontrol.ini", QSettings::IniFormat);

    if (parser.isSet("calibrate-accelgyro")) {
        return calibrateTemperatureDependency(settings, sensors);
    } else if (parser.isSet("calibrate-magnetometer")) {
        return calibrateMagnetometer(settings, sensors);
    } else if (parser.isSet("test-rotors")) {
        return testRotors(rotorController);
    } else if (parser.isSet("const-throttle")) {
        float v = parser.value("const-throttle").toFloat();
        v = qBound(0.f, v, 1.f);
        rotorController->setThrottle({v, v, v, v});
        rotorController->setEnabled(true);
        return app.exec();
    }

    loadCalibration(settings, sensors);

    regulator->restoreSettings(settings);

    ElapsedTimer timer;
    timer.start();

    auto update = [&]() {
        if (timer.elapsed() < 20/* || !regulator->isActive()*/) {
            return;
        }
        timer.restart();
        auto angles = sensors->angles();
//        printf("\rRoll: %.2f       Pitch:%.2f       Yaw:%.2f      Vz: %.2f    ", angles[0], angles[1], angles[2], sensors->climbRate());
//        const auto output = regulator->angularRatePidOutput();
//        const auto angleOutput = regulator->anglePidOutput();
//        printf("  Angular velocity PID: %.2f %.2f %.2f\n", output[0], output[1], output[2]);
//        printf("  Angle PID: %.2f %.2f\n", angleOutput[0], angleOutput[1]);
        fflush(stdout);
    };

    // Failsafe
    QObject::connect(sensors, &Sensors::anglesChanged, regulator, [&](const arma::fvec3 &angles) {
        constexpr float CritAngle = 45;
        if (regulator->isActive() && any(abs(angles(arma::span(0, 1))) > CritAngle)) {
            qDebug() << "Failsafe triggered!" << sensors->angles();
            regulator->setActive(false);
        }
    });

    QObject::connect(sensors, &Sensors::updateComplete, &app, update);

    OscillationDetector angleOscDet;
    QObject::connect(sensors, &Sensors::anglesChanged,
                     &angleOscDet, &OscillationDetector::handleNewValue,
                     Qt::DirectConnection);

    TuningInterface tuningInterface;
    tuningInterface.setSensors(sensors);
    tuningInterface.setFlightRegulator(regulator);

    std::unique_ptr<QWebChannel> webChannel{createWebChannel(nullptr)};
    webChannel->registerObject("tuning", &tuningInterface);

    Receiver receiver;

    QTimer::singleShot(1200, regulator, [&]() {
        receiver.attachToRegulator(regulator);
    });

    int retCode = app.exec();
    qDebug() << "\nExiting.";
    rtThread.quit();
    rtThread.wait();
    regulator->storeSettings(settings);
    return retCode;
}
