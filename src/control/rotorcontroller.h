#ifndef ROTORCONTROLLER_H
#define ROTORCONTROLLER_H

#include <QObject>
#include <armadillo>

#include "config.h"

#if COPTERCONTROL_SIMULATOR
#include "math_util.h"

namespace StepCore {
class World;
class Item;
}
#endif

class RotorController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(arma::fvec4 throttle READ throttle WRITE setThrottle NOTIFY throttleChanged)
    Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled NOTIFY enabledChanged)
public:
    enum Rotor {
        FrontLeft = 0,
        FrontRight,
        RearLeft,
        RearRight,

        RotorCount
    };
    Q_ENUM(Rotor)

    explicit RotorController(QObject *parent = nullptr);
    ~RotorController();

#if COPTERCONTROL_SIMULATOR
    StepCore::Item *item() const { return m_item; }
    void setItem(StepCore::Item *item) { m_item = item; }
#endif

    arma::fvec4 throttle() const;
    float throttle(Rotor rotor) const;
    bool isEnabled() const;

signals:
    void throttleChanged(const arma::fvec4 &throttle);
    void enabledChanged(bool enabled);

public slots:
    void setThrottle(const arma::fvec4 &throttle);
    void setThrottle(Rotor rotor, float throttle);
    void setEnabled(bool enabled);

private:
    void updateOutput();

#if COPTERCONTROL_SIMULATOR
    StepCore::Item *m_item = nullptr;

    std::array<RCFilter<float>, 2> m_forceLatency;
#endif

    arma::fvec4 m_throttle { arma::fill::zeros };
    bool m_enabled = false;
};

int testRotors(RotorController *controller);

#endif // ROTORCONTROLLER_H
