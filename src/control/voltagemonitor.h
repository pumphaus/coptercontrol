#ifndef VOLTAGEMONITOR_H
#define VOLTAGEMONITOR_H

#include <QObject>
#include "elapsedtimer.h"

class VoltageMonitor : public QObject
{
    Q_OBJECT
    Q_PROPERTY(float voltage READ voltage NOTIFY voltageChanged)

public:
    explicit VoltageMonitor(QObject *parent = nullptr);

    float voltage() const;

signals:

void voltageChanged(float voltage);

public slots:
    void initiateUpdate();

private:
    ElapsedTimer m_rateReduceTimer;
    float m_voltage;
};

#endif // VOLTAGEMONITOR_H
