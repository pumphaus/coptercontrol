#include "voltagemonitor.h"

#include <QCoreApplication>
#include <QProcess>
#include <QtDebug>

namespace {

// Invocation of tool:
// # ./voltmon 7
// 0.00

QString executable()
{
    static QString executablePath = qApp->applicationDirPath() + "/voltmon";
    return executablePath;
}

int channel = 7;
}

VoltageMonitor::VoltageMonitor(QObject *parent)
    : QObject(parent)
{
}

float VoltageMonitor::voltage() const
{
    return m_voltage;
}

void VoltageMonitor::initiateUpdate()
{
    if (m_rateReduceTimer.isValid() && m_rateReduceTimer.elapsed() < 1000) {
        return;
    }
    m_rateReduceTimer.restart();

    QProcess *process = new QProcess;
    process->setProgram(executable());
    process->setArguments({QString::number(channel)});
    process->setReadChannel(QProcess::StandardOutput);
    connect(process, &QProcess::finished, this, [=]() {
        auto output = process->readAll().trimmed();
        process->deleteLater();

        m_voltage = output.toFloat();
        emit voltageChanged(m_voltage);
    });
    connect(process, &QProcess::errorOccurred, process, &QProcess::deleteLater);

    process->start();
}
