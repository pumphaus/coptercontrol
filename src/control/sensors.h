#ifndef SENSORS_H
#define SENSORS_H

#include <QBasicTimer>
#include <QObject>
#include <QVariant>
#include <QVector>
#include <armadillo>
#include <memory>
#include <functional>
#include <optional>
#include <random>

#include "config.h"

class QSettings;

class EdgeDetector;
class MPU6050;
class HMC5883L;
class MS561101BA;
class Rcw0001;
class VoltageMonitor;

class ClimbRateCalculator;

struct BarometerReading
{
    float pressure;
    float temperature;
};

#if COPTERCONTROL_SIMULATOR
namespace StepCore {
class Item;
class World;
}
#endif

class Sensors : public QObject
{
    Q_OBJECT
    Q_PROPERTY(arma::fvec3 acceleration READ acceleration NOTIFY accelerationChanged)
    Q_PROPERTY(arma::fvec3 uncalibratedAcceleration READ uncalibratedAcceleration NOTIFY uncalibratedAccelerationChanged)
    Q_PROPERTY(arma::fvec3 angularVelocity READ angularVelocity NOTIFY angularVelocityChanged)
    Q_PROPERTY(arma::fvec3 uncalibratedAngularVelocity READ uncalibratedAngularVelocity NOTIFY uncalibratedAngularVelocityChanged)
    Q_PROPERTY(arma::fvec3 heading READ heading NOTIFY headingChanged)
    Q_PROPERTY(arma::fvec3 uncalibratedHeading READ uncalibratedHeading NOTIFY uncalibratedHeadingChanged)
    Q_PROPERTY(QVariant accelerationOffset READ accelerationOffset WRITE setAccelerationOffset NOTIFY accelerationOffsetChanged)
    Q_PROPERTY(QVariant angularVelocityOffset READ angularVelocityOffset WRITE setAngularVelocityOffset NOTIFY angularVelocityOffsetChanged)
    Q_PROPERTY(arma::fvec3 headingOffset READ headingOffset WRITE setHeadingOffset NOTIFY headingOffsetChanged)
    Q_PROPERTY(arma::fvec3 headingScale READ headingScale WRITE setHeadingScale NOTIFY headingScaleChanged)
    Q_PROPERTY(arma::fvec3 angles READ angles NOTIFY anglesChanged)
    Q_PROPERTY(float roll READ roll NOTIFY rollChanged)
    Q_PROPERTY(float pitch READ pitch NOTIFY pitchChanged)
    Q_PROPERTY(float yaw READ yaw NOTIFY yawChanged)
    Q_PROPERTY(arma::fmat33 rotationMatrix READ rotationMatrix NOTIFY rotationMatrixChanged)
    Q_PROPERTY(float relativeAltitude READ relativeAltitude NOTIFY relativeAltitudeChanged)
    Q_PROPERTY(float absoluteAltitude READ absoluteAltitude NOTIFY absoluteAltitudeChanged)
    Q_PROPERTY(float climbRate READ climbRate NOTIFY climbRateChanged)
    Q_PROPERTY(float climbAcceleration READ climbAcceleration NOTIFY climbAccelerationChanged)
    Q_PROPERTY(float voltage READ voltage NOTIFY voltageChanged)
    Q_PROPERTY(float pressure READ pressure NOTIFY pressureChanged)
    Q_PROPERTY(float temperature READ temperature NOTIFY temperatureChanged)
    Q_PROPERTY(float imuTemperature READ imuTemperature NOTIFY imuTemperatureChanged)
public:
    explicit Sensors(QObject *parent = nullptr);
    ~Sensors();

#if COPTERCONTROL_SIMULATOR
    StepCore::Item *item() const { return m_item; }
    void setItem(StepCore::Item *item) { m_item = item; }
#endif

    arma::fvec3 acceleration() const;
    arma::fvec3 uncalibratedAcceleration() const;
    arma::fvec3 angularVelocity() const;
    arma::fvec3 uncalibratedAngularVelocity() const;
    arma::fvec3 heading() const;
    arma::fvec3 uncalibratedHeading() const;
    arma::fvec3 angles() const;
    float roll() const;
    float pitch() const;
    float yaw() const;
    arma::fmat33 rotationMatrix() const;

    QVariant accelerationOffset() const;
    arma::fvec3 currentAccelerationOffset() const;
    QVariant angularVelocityOffset() const;
    arma::fvec3 currentAngularVelocityOffset() const;
    arma::fvec3 headingOffset() const;
    arma::fvec3 headingScale() const;

    float relativeAltitude() const;
    float climbRate() const;
    float climbAcceleration() const;
    float voltage() const;

    float pressure() const;
    float temperature() const;
    float imuTemperature() const;
    float absoluteAltitude() const;

public slots:
    void update();

    void setAccelerationOffset(const QVariant &accelerationOffset);
    void setAngularVelocityOffset(const QVariant &angularVelocityOffset);
    void setHeadingOffset(const arma::fvec3 &headingOffset);
    void setHeadingScale(const arma::fvec3 &headingScale);

signals:
    void accelerationChanged(const arma::fvec3 &acceleration);
    void uncalibratedAccelerationChanged(const arma::fvec3 &uncalibratedAcceleration);
    void angularVelocityChanged(const arma::fvec3 &angularVelocity);
    void uncalibratedAngularVelocityChanged(const arma::fvec3 &uncalibratedAngularVelocity);
    void anglesChanged(const arma::fvec3 &angles);
    void rollChanged(float roll);
    void pitchChanged(float pitch);
    void yawChanged(float yaw);
    void rotationMatrixChanged(const arma::fmat33 &rotationMatrix);
    void accelerationOffsetChanged(const QVariant &accelerationOffset);
    void angularVelocityOffsetChanged(const QVariant &angularVelocityOffset);
    void headingChanged(const arma::fvec3 &heading);
    void uncalibratedHeadingChanged(const arma::fvec3 &uncalibratedHeading);
    void headingOffsetChanged(const arma::fvec3 &headingOffset);
    void headingScaleChanged(const arma::fvec3 &headingScale);
    void relativeAltitudeChanged(float relativeAltitude);
    void absoluteAltitudeChanged(float absoluteAltitude);
    void climbRateChanged(float climbRate);
    void climbAccelerationChanged(float climbAcceleration);
    void pressureChanged(float pressure);
    void temperatureChanged(float temperature);
    void imuTemperatureChanged(float imuTemperature);

    void updateComplete();

    void voltageChanged(float voltage);

protected:
    void timerEvent(QTimerEvent *event) override;

private:
    void updateAltitude();

    arma::fvec3 m_uncalibratedAcceleration { arma::fill::zeros };
    arma::fvec3 m_uncalibratedAngularVelocity { arma::fill::zeros };
    arma::fvec3 m_uncalibratedHeading { arma::fill::zeros };
    arma::fvec3 m_acceleration { arma::fill::zeros };
    arma::fvec3 m_angularVelocity { arma::fill::zeros };
    arma::fvec3 m_heading { arma::fill::zeros };

    arma::fvec3 m_angles { arma::fill::zeros };
    arma::fmat33 m_rotationMatrix { arma::fill::zeros };

    QVariant m_accelerationOffset;
    QVariant m_angularVelocityOffset;
    arma::fvec3 m_headingOffset { arma::fill::zeros };
    arma::fvec3 m_headingScale { arma::fill::ones };

    float m_relativeAltitude = 0;
    float m_climbRate = 0;
    float m_climbAcceleration = 0;
    float m_pressure = 0;
    float m_temperature = 0;
    float m_imuTemperature = 0;
    float m_absoluteAltitude = 0;

#if COPTERCONTROL_SIMULATOR
    StepCore::Item *m_item = nullptr;

    std::mt19937 m_mtgen;
    std::normal_distribution<float> m_angularVelocityDist {0.f, 0.039f};
    std::normal_distribution<float> m_angleDist{0.f, 0.01f};
#else
    std::unique_ptr<MPU6050> m_accelgyro;
    std::unique_ptr<HMC5883L> m_mag;
    std::unique_ptr<MS561101BA> m_baro;
    Rcw0001 *m_dist = nullptr;
    VoltageMonitor *m_voltMon = nullptr;
    std::function<arma::fvec3(const arma::fvec3&)> m_angularVelocityFilter;
    std::function<float(float)> m_relativeAltitudeCalc;
    std::function<float(float)> m_absoluteAltitudeCalc;
    std::function<std::optional<BarometerReading>()> m_baroSampler;
    std::unique_ptr<ClimbRateCalculator> m_climbRateCalc;
#endif

    QBasicTimer m_interruptPollTimer;
};

int calibrateTemperatureDependency(QSettings &settings, Sensors *sensors);
int calibrateMagnetometer(QSettings &settings, Sensors *sensors);
void loadCalibration(QSettings &settings, Sensors *sensors);

#endif // SENSORS_H
