#ifndef FLIGHTREGULATOR_H
#define FLIGHTREGULATOR_H

#include <QObject>
#include <QVariant>

#include <armadillo>

#include <memory>

#include "pid.h"

class QSettings;

class RotorController;
class Sensors;

class FlightRegulator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool active READ isActive WRITE setActive NOTIFY activeChanged)
    Q_PROPERTY(FlightRegulator::Mode mode READ mode WRITE setMode NOTIFY modeChanged)
    Q_PROPERTY(arma::fvec3 angularRatePidOutput READ angularRatePidOutput NOTIFY angularRatePidOutputChanged)
    Q_PROPERTY(arma::fvec2 anglePidOutput READ anglePidOutput NOTIFY anglePidOutputChanged)
    Q_PROPERTY(float climbRatePidOutput READ climbRatePidOutput NOTIFY climbRatePidOutputChanged)

    Q_PROPERTY(float climbRateSetpoint READ climbRateSetpoint WRITE setClimbRateSetpoint NOTIFY climbRateSetpointChanged)
    Q_PROPERTY(arma::fvec2 angleSetpoint READ angleSetpoint WRITE setAngleSetpoint NOTIFY angleSetpointChanged)
    Q_PROPERTY(arma::fvec3 angularRateSetpoint READ angularRateSetpoint WRITE setAngularRateSetpoint NOTIFY angularRateSetpointChanged)
public:
    enum Mode {
        RateMode = 0,
        AngleMode,
    };

    explicit FlightRegulator(QObject *parent = nullptr);

    void setRotorController(RotorController *rotors);
    RotorController *rotorController() const { return m_rotorController; }

    void setSensors(Sensors *sensors);
    Sensors *sensors() const { return m_sensors; }

    bool isActive() const;
    arma::fvec3 angularRatePidOutput() const;
    arma::fvec2 anglePidOutput() const;
    float climbRatePidOutput() const;

    float climbRateSetpoint() const;
    arma::fvec2 angleSetpoint() const;
    arma::fvec3 angularRateSetpoint() const;

    void copyStateTo(FlightRegulator *other);

    Q_INVOKABLE QVariantMap pidParams() const;
    Q_INVOKABLE bool setPidParam(const QString &pidName, int subIndex, const QByteArray &paramName, const QVariant &value);

    void storeSettings(QSettings &settings) const;
    void restoreSettings(QSettings &settings);

    FlightRegulator::Mode mode() const;
    void setMode(FlightRegulator::Mode newMode);

public slots:
    void setActive(bool active);
    void setClimbRateSetpoint(float climbRateSetpoint);
    void setAngleSetpoint(const arma::fvec2 &angleSetpoint);
    void setAngularRateSetpoint(const arma::fvec3 &angularRateSetpoint);

signals:
    void activeChanged(bool active);
    void modeChanged(FlightRegulator::Mode mode);
    void angularRatePidOutputChanged(const arma::fvec3 &angularRatePidOutput);
    void anglePidOutputChanged(const arma::fvec2 &anglePidOutput);
    void climbRatePidOutputChanged(float climbRatePidOutput);

    void climbRateSetpointChanged(float climbRateSetpoint);
    void angleSetpointChanged(const arma::fvec2 &angleSetpoint);
    void angularRateSetpointChanged(const arma::fvec3 &angularRateSetpoint);

    void updateComplete();

private slots:
    void update();
    void updateClimbRateOutput();

private:
    void setPidDerivativesBySensors();

    RotorController *m_rotorController = nullptr;
    Sensors *m_sensors = nullptr;

    enum AngleIndex {
        YawIndex = 2,
        PitchIndex = 1,
        RollIndex = 0,
    };

    struct {
        std::array<PID, 3> angularRatePids;
        std::array<PID, 2> absoluteAnglePids;
        PID climbRatePid;
        bool active = false;
        FlightRegulator::Mode mode = RateMode;
        arma::fvec3 angularRatePidOutput { arma::fill::zeros };
        arma::fvec2 absoluteAnglePidOutput { arma::fill::zeros };
        float climbRatePidOutput = 0;
        arma::fvec2 angleSetpoint { arma::fill::zeros };
        arma::fvec3 angularRateSetpoint { arma::fill::zeros };
    } s;
};

#endif // FLIGHTREGULATOR_H
