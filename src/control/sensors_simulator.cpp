#include "sensors.h"

#include "math_util.h"

#include <stepcore/rigidbody.h>
#include <stepcore/world.h>

namespace {
const QString CopterBodyName = "copter";
}

enum AngleIndex {
    YawIndex = 2,
    PitchIndex = 1,
    RollIndex = 0,
};

Sensors::Sensors(QObject *parent)
    : QObject(parent)
{
    qRegisterMetaType<arma::fvec>("arma::fvec");
    qRegisterMetaType<arma::fvec3>("arma::fvec3");
    qRegisterMetaType<arma::fmat33>("arma::fmat33");
    qRegisterMetaType<arma::fmat>("arma::fmat");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QMetaType::registerDebugStreamOperator<arma::fvec>();
    QMetaType::registerDebugStreamOperator<arma::fvec3>();
    QMetaType::registerDebugStreamOperator<arma::fmat33>();
    QMetaType::registerDebugStreamOperator<arma::fmat>();
#endif
}

Sensors::~Sensors() {}

arma::fvec3 Sensors::acceleration() const
{
    return m_acceleration;
}

float Sensors::pitch() const
{
    return m_angles[PitchIndex];
}

float Sensors::roll() const
{
    return m_angles[RollIndex];
}

float Sensors::yaw() const
{
    return m_angles[YawIndex];
}

arma::fmat33 Sensors::rotationMatrix() const
{
    return m_rotationMatrix;
}

arma::fvec3 Sensors::currentAccelerationOffset() const
{
    return arma::fvec3{arma::fill::zeros};
}

arma::fvec3 Sensors::currentAngularVelocityOffset() const
{
    return arma::fvec3{arma::fill::zeros};
}

QVariant Sensors::accelerationOffset() const
{
    return m_accelerationOffset;
}

QVariant Sensors::angularVelocityOffset() const
{
    return m_angularVelocityOffset;
}

arma::fvec3 Sensors::headingOffset() const
{
    return m_headingOffset;
}

arma::fvec3 Sensors::headingScale() const
{
    return m_headingScale;
}

float Sensors::relativeAltitude() const
{
    return m_relativeAltitude;
}

float Sensors::climbRate() const
{
    return m_climbRate;
}

float Sensors::climbAcceleration() const
{
    return m_climbAcceleration;
}

float Sensors::voltage() const
{
    return 0;
}

float Sensors::pressure() const
{
    return m_pressure;
}

float Sensors::temperature() const
{
    return m_temperature;
}

float Sensors::imuTemperature() const
{
    return m_imuTemperature;
}

float Sensors::absoluteAltitude() const
{
    return m_absoluteAltitude;
}

arma::fvec3 Sensors::angularVelocity() const
{
    return m_angularVelocity;
}

arma::fvec3 Sensors::uncalibratedAcceleration() const
{
    return m_uncalibratedAcceleration;
}

arma::fvec3 Sensors::uncalibratedAngularVelocity() const
{
    return m_uncalibratedAngularVelocity;
}

arma::fvec3 Sensors::heading() const
{
    return m_heading;
}

arma::fvec3 Sensors::uncalibratedHeading() const
{
    return m_uncalibratedHeading;
}

arma::fvec3 Sensors::angles() const
{
    return m_angles;
}

void Sensors::update()
{
    if (!item() || !item()->world()) {
        return;
    }

    auto copterBody = std::find_if(item()->world()->bodies().begin(),
                                   item()->world()->bodies().end(),
                                   [](auto &&body)
    {
        return body->name() == CopterBodyName;
    });
    if (copterBody == item()->world()->bodies().end()) {
        qWarning() << "Failed to find copter body";
        return;
    }

    auto *rigidBody = static_cast<StepCore::RigidBody*>(*copterBody);

    m_angularVelocity[RollIndex] = -rigidBody->angularVelocity() * 180 / M_PI
            + m_angularVelocityDist(m_mtgen);
    emit angularVelocityChanged(m_angularVelocity);

    m_angles[RollIndex] = -rigidBody->angle() * 180 / M_PI
            + m_angleDist(m_mtgen);
    emit anglesChanged(m_angles);

    m_climbRate = rigidBody->velocity()[1];
    emit climbRateChanged(m_climbRate);

    // Signal that this round of updates is complete
    emit updateComplete();
}

void Sensors::setAccelerationOffset(const QVariant &accelerationOffset)
{
    if (m_accelerationOffset == accelerationOffset)
        return;

    m_accelerationOffset = accelerationOffset;
    emit accelerationOffsetChanged(m_accelerationOffset);
}

void Sensors::setAngularVelocityOffset(const QVariant &angularVelocityOffset)
{
    if (m_angularVelocityOffset == angularVelocityOffset)
        return;

    m_angularVelocityOffset = angularVelocityOffset;
    emit angularVelocityOffsetChanged(m_angularVelocityOffset);
}

void Sensors::setHeadingOffset(const arma::fvec3 &headingOffset)
{
    if (all(m_headingOffset == headingOffset)) {
        return;
    }

    m_headingOffset = headingOffset;
    emit headingOffsetChanged(m_headingOffset);
}

void Sensors::setHeadingScale(const arma::fvec3 &headingScale)
{
    if (all(m_headingScale == headingScale)) {
        return;
    }

    m_headingScale = headingScale;
    emit headingScaleChanged(m_headingScale);
}

void Sensors::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);
}

void Sensors::updateAltitude()
{
}
