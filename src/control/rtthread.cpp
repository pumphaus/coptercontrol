#include "rtthread.h"

#include <QFile>

#include <sched.h>

constexpr char SchedRtRuntimeFile[] = "/proc/sys/kernel/sched_rt_runtime_us";
const QByteArray SchedRtRuntime = "-1\n";

RTThread::RTThread(QObject *parent)
    : QThread(parent)
{

}

RTThread::~RTThread()
{
    if (!isRunning() || isFinished()) {
        return;
    }
    quit();
    wait(1000);
    if (!isFinished()) {
        terminate();
    }
}

void RTThread::run()
{
    // Allow unlimited cpu usage
    {
        QFile file(SchedRtRuntimeFile);
        if (!file.open(QFile::WriteOnly)) {
            qFatal("Failed to open \"%s\"!", SchedRtRuntimeFile);
        }
        if (file.write(SchedRtRuntime) != SchedRtRuntime.length()) {
            qFatal("Failed to write \"%s\" to \"%s\"!",
                   qUtf8Printable(SchedRtRuntime), SchedRtRuntimeFile);
        }
    }

    sched_param param;
    memset(&param, 0, sizeof param);
    param.sched_priority = 99;
    if (sched_setscheduler(0, SCHED_FIFO, &param) < 0) {
        qFatal("Failed to set FIFO scheduler for the thread!");
    }

    // Execute the QThread normally
    exec();
}
