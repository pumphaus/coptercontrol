#ifndef RECEIVER_H
#define RECEIVER_H

#include <QObject>
#include <armadillo>

class FlightRegulator;
class QUdpSocket;

class Receiver : public QObject
{
    Q_OBJECT
    Q_PROPERTY(arma::fvec3 angularRate READ angularRate NOTIFY angularRateChanged)
    Q_PROPERTY(arma::fvec2 angles READ angles NOTIFY anglesChanged)
    Q_PROPERTY(float climbRate READ climbRate NOTIFY climbRateChanged)
    Q_PROPERTY(bool regulatorEnabled READ isRegulatorEnabled NOTIFY regulatorEnabledChanged)
public:
    explicit Receiver(QObject *parent = nullptr);

    arma::fvec3 angularRate() const;
    arma::fvec2 angles() const;
    float climbRate() const;
    bool isRegulatorEnabled() const;

    void attachToRegulator(FlightRegulator *regulator);

signals:
    void angularRateChanged(const arma::fvec3 &angularRate);
    void anglesChanged(const arma::fvec2 &angles);
    void climbRateChanged(float climbRate);
    void regulatorEnabledChanged(bool regulatorEnabled);

public slots:

private:
    void broadcastHeartbeat();
    void readDatagrams();
    void processDatagram(const QByteArray &data);

    arma::fvec3 m_angularRate{arma::fill::zeros};
    arma::fvec2 m_angles{arma::fill::zeros};
    float m_climbRate = 0;
    bool m_regulatorEnabled = false;
    QUdpSocket *m_socket = nullptr;
    quint64 m_lastTimeStamp = 0;
};

#endif // RECEIVER_H
