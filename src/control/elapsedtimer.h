#ifndef ELAPSEDTIMER_H
#define ELAPSEDTIMER_H

#include <QElapsedTimer>
#include <cmath>
#include "config.h"

#if !COPTERCONTROL_SIMULATOR
using ElapsedTimer = QElapsedTimer;
#else

class ElapsedTimer
{
public:
    static double globalTime;

    ElapsedTimer() {}

    static QElapsedTimer::ClockType clockType() noexcept { return QElapsedTimer::MonotonicClock; }
    static bool isMonotonic() noexcept { return true; }

    void start() noexcept { t = globalTime; }
    qint64 restart() noexcept { auto dt = elapsed(); start(); return dt; }
    void invalidate() noexcept { t = -1; }
    bool isValid() const noexcept { return t >= 0; }

    qint64 nsecsElapsed() const noexcept { return std::llround((globalTime - t) * 1e9); }
    qint64 elapsed() const noexcept { return std::llround((globalTime - t) * 1e3); }
    bool hasExpired(qint64 timeout) const noexcept;

    qint64 msecsSinceReference() const noexcept { return std::llround(t * 1e3); }
    qint64 msecsTo(const ElapsedTimer &other) const noexcept { return std::llround((other.t - t) * 1e3); }
    qint64 secsTo(const ElapsedTimer &other) const noexcept { return std::llround(other.t - t); }

    bool operator==(const ElapsedTimer &other) const noexcept
    { return t == other.t; }
    bool operator!=(const ElapsedTimer &other) const noexcept
    { return !(*this == other); }

    friend bool operator<(const ElapsedTimer &v1, const ElapsedTimer &v2) noexcept {
        return v1.t < v2.t;
    }

private:
    double t = -1;
};

#endif

#endif // ELAPSEDTIMER_H
