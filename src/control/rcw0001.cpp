#include "rcw0001.h"

#include <QTimerEvent>
#include <QtDebug>

#include "blasterpp/blasterpp.h"

using namespace BlasterPP;
using namespace std::chrono_literals;

namespace {
enum DmaChannelId {
    OutputChannel = 0,
    InputChannel = 1
};

constexpr unsigned int TriggerPin = 20;
constexpr unsigned int EchoPin = 16;
constexpr float SpeedOfSound = 340.29f;

DmaChannel *dmaChannel()
{
    static DmaChannel channel(11, 15000, 2us, 2, InputChannel,
                              DmaChannel::DelayViaPcm);
    static bool setup = false;

    if (!setup) {
        setGpioMode(TriggerPin, ModeOutput);
        setGpioMode(EchoPin, ModeInput);

        channel.setPwmPattern(OutputChannel);
        channel.setPulseWidth(OutputChannel, TriggerPin, 10us);
        channel.start();
        setup = true;
    }

    return &channel;
}

template<class T>
void circularIterate(int firstId, int lastId, T &&fn)
{
    const int N = dmaChannel()->sampleCount();

    auto delta = lastId - firstId;
    if (delta < 0) {
        delta += N;
    }
    ++delta;

    for (int i = 0; i < delta; ++i) {
        const int sampleIdx = (firstId + i) % N;
        fn(sampleIdx);
    }
}

struct EdgeDetector
{
    enum Type {
        Rising,
        Falling
    };

    struct Edge {
        int pos;
        Type type;
    };

    EdgeDetector(const tcb::span<quint32> &samples, int mask = 1)
        : samples(samples), mask(mask)
    {
    }

    const tcb::span<quint32> samples;
    const int mask;
    bool wasHigh = false;
    bool initialized = false;

    std::vector<Edge> edges;

    bool isHigh(quint32 samp) const { return samp & mask; }

    void operator()(int idx) {
        const bool nowHigh = isHigh(samples[idx]);

        if (!initialized) {
            wasHigh = nowHigh;
            initialized = true;
            return;
        }

        if (nowHigh && !wasHigh) {
            edges.push_back({idx, Rising});
        } else if (wasHigh && !nowHigh) {
            edges.push_back({idx, Falling});
        }

        wasHigh = nowHigh;
    }
};

}

Rcw0001::Rcw0001(QObject *parent)
    : QObject(parent)
{
    setAutoProcessSamples(true);
}

float Rcw0001::distance() const
{
    return m_distance;
}

void Rcw0001::setAutoProcessSamples(bool autoProcess)
{
    if (isAutoProcessingSamples() == autoProcess) {
        return;
    }

    if (autoProcess) {
        m_readoutTimer.start(5, Qt::PreciseTimer, this);
    } else {
        m_readoutTimer.stop();
    }
}

bool Rcw0001::isAutoProcessingSamples() const
{
    return m_readoutTimer.isActive();
}

float Rcw0001::cycleTime() const
{
    using namespace std::chrono;
    return duration_cast<duration<float>>(dmaChannel()->cycleTime()).count();
}

void Rcw0001::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == m_readoutTimer.timerId()) {
        processSamples();
    }
}

void Rcw0001::processSamples()
{
    const int curSampleId = dmaChannel()->currentSampleIndex();
    const bool wrapsAround = curSampleId < m_lastPos;

    const int endSampleId = wrapsAround ? dmaChannel()->sampleCount()
                                        : curSampleId + 1;

    EdgeDetector detector(dmaChannel()->samples(InputChannel), 1 << EchoPin);
    for (int i = m_lastPos; i < endSampleId; ++i) {
        detector(i);
    }
    m_lastPos = wrapsAround ? 0 : endSampleId - 1;

    int risingEdge = m_lastRisingEdgeId;
    int fallingEdge = -1;

    for (const auto &edge : detector.edges) {
        if (edge.type == EdgeDetector::Rising) {
            risingEdge = edge.pos;
        } else {
            fallingEdge = edge.pos;
        }
    }

    if (risingEdge > -1 && fallingEdge > -1) {
        m_lastRisingEdgeId = -1;
        updateDistance(fallingEdge - risingEdge);
    } else {
        m_lastRisingEdgeId = risingEdge;
    }

    // We've reached the end of one recording period and didn't find a
    // falling edge
    if (endSampleId == dmaChannel()->sampleCount()
            && m_lastRisingEdgeId != -1)
    {
        signalInvalidMeasurement();
    }

    // The index has wrapped and we need to do another update
    if (wrapsAround) {
        processSamples();
    }
}

void Rcw0001::updateDistance(int sampleDistance)
{
    using FpSeconds = std::chrono::duration<double>;

    const FpSeconds dt = sampleDistance * dmaChannel()->sampleTime();
    const auto dist = dt.count() * SpeedOfSound / 2;

    m_distance = dist;
    emit distanceChanged(m_distance);
}

void Rcw0001::signalInvalidMeasurement()
{
    m_distance = std::numeric_limits<float>::infinity();
    emit distanceChanged(m_distance);
}
