#include "stephook.h"

#include <QCoreApplication>
#include <QSettings>
#include <QTimer>
#include <QThread>
#include <QWebChannel>
#include <QtDebug>

#include <step/mainwindow.h>
#include <step/worldfactory.h>
#include <step/worldmodel.h>
#include <stepcore/world.h>

#include <memory>

#include "elapsedtimer.h"
#include "flightregulator.h"
#include "receiver.h"
#include "rotorcontroller.h"
#include "sensors.h"
#include "tuninginterface.h"

TuningInterface *tuningInterface = nullptr;
Receiver *receiver = nullptr;

class CopterControl : public StepCore::Item, public StepCore::Tool
{
    STEPCORE_OBJECT(CopterControl)

public:
    CopterControl(const QString &name = QString{})
        : StepCore::Item(name)
    {
        init();
        timer.start();
        flightRegulator->setActive(true);
        QSettings settings("coptercontrol.ini", QSettings::IniFormat);
        flightRegulator->restoreSettings(settings);

        if (!tuningInterface->flightRegulator()) {
            tuningInterface->setFlightRegulator(flightRegulator);
        }
    }

    CopterControl(const CopterControl &other)
        : StepCore::Item(other)
    {
        timer = other.timer;
        init();
        other.flightRegulator->copyStateTo(flightRegulator);
    }

    void init()
    {
        sensors = new Sensors(q.get());
        rotorController = new RotorController(q.get());
        flightRegulator = new FlightRegulator(q.get());

        flightRegulator->setSensors(sensors);
        flightRegulator->setRotorController(rotorController);
    }

    void moveToCorrectThread()
    {
        auto simThread = QThread::currentThread();
        if (simThread != q->thread()) {
            QMetaObject::invokeMethod(q.get(), [=] {
                q->moveToThread(simThread);
            }, Qt::BlockingQueuedConnection);
        }
    }

    void evolveHook(double time) override
    {
        moveToCorrectThread();
        tuningInterface->setSensors(sensors);
        tuningInterface->setFlightRegulator(flightRegulator);
        receiver->attachToRegulator(flightRegulator);
        QCoreApplication::processEvents();

        rotorController->setItem(this);
        sensors->setItem(this);
        auto unsetItems = qScopeGuard([&] {
            rotorController->setItem(nullptr);
            sensors->setItem(nullptr);
        });

        ElapsedTimer::globalTime = time;

        if (timer.elapsed() < 2) {
            return;
        }
        timer.restart();

        sensors->update();
    }

private:
    std::unique_ptr<QObject> q       = std::make_unique<QObject>();
    Sensors         *sensors         = nullptr;
    RotorController *rotorController = nullptr;
    FlightRegulator *flightRegulator = nullptr;
    ElapsedTimer    timer;
};

STEPCORE_META_OBJECT(CopterControl, "", "", 0,
    STEPCORE_SUPER_CLASS(Item) STEPCORE_SUPER_CLASS(Tool),)

int _dummy = [] {
    WorldFactory::extraMetaObjects().append(CopterControl::staticMetaObject());
    return 0;
}();

void startupHook(MainWindow *window)
{
    receiver = new Receiver(window);

    auto *channel = createWebChannel(window);
    tuningInterface = new TuningInterface(window);
    channel->registerObject("tuning", tuningInterface);

    QObject::connect(tuningInterface, &QObject::destroyed, [] {
        if (!tuningInterface->flightRegulator()) {
            return;
        }
        QSettings settings("coptercontrol.ini", QSettings::IniFormat);
        tuningInterface->flightRegulator()->storeSettings(settings);
    });

    auto *model = window->findChild<WorldModel*>();
    qDebug() << "window" << window;
    qDebug() << "Model:" << model;
    model->setSimulationFps(25);
    model->setSimulationSubsteps(20);

//    QObject::connect(model, &WorldModel::modelReset, [model] {
//        model->addItem(new CopterControl("CopterControl"));
//        qDebug() << "Items:";
//        for (auto *item : model->world()->items()) {
//            qDebug() << "item:" << item->name() << item->metaObject()->className();
//        }
//    });
//    model->world()->addItem(new CopterControl);
//    model->world()->setEvolveHook(evolveHook);
}
