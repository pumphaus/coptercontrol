#ifndef RTTHREAD_H
#define RTTHREAD_H

#include <QThread>

class RTThread : public QThread
{
public:
    explicit RTThread(QObject *parent = nullptr);
    ~RTThread();

protected:
    void run() override;
};

#endif // RTTHREAD_H
