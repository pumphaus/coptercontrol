#include "oscillationdetector.h"

#include <QtDebug>
#include <QDateTime>
#include <QThread>
#include <QTimer>

using namespace arma;

namespace {
template<class T>
T hann(uint N)
{
    const T n = regspace<T>(0, N - 1);
    const T W = sin(M_PI * n / (N - 1));
    return W % W;
}
}

OscillationDetector::OscillationDetector(QObject *parent)
    : QObject(parent)
{
    setBufferSize(2048);
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &OscillationDetector::checkOscillations);
    timer->start(500);
}

int OscillationDetector::bufferSize() const
{
    return m_buffer.n_rows;
}

float OscillationDetector::criticalFrequency() const
{
    return m_criticalFrequency;
}

float OscillationDetector::criticalAmplitude() const
{
    return m_criticalAmplitude;
}

void OscillationDetector::handleNewValue(const arma::fvec &value)
{
    const auto timestamp = QDateTime::currentMSecsSinceEpoch() * 1e-3;

    if (QThread::currentThread() != thread()) {
        QMetaObject::invokeMethod(this, [=]() {
            pushValue(timestamp, value);
        }, Qt::QueuedConnection);
    } else {
        pushValue(timestamp, value);
    }
}

void OscillationDetector::setBufferSize(int bufferSize)
{
    if (this->bufferSize() == bufferSize) {
        return;
    }

    m_buffer.zeros(bufferSize, 0);
    m_timestamps.zeros(bufferSize);
    m_bufferPos = 0;

    emit bufferSizeChanged(this->bufferSize());
}

void OscillationDetector::setCriticalFrequency(float criticalFrequency)
{
    if (qFuzzyCompare(m_criticalFrequency, criticalFrequency)) {
        return;
    }

    m_criticalFrequency = criticalFrequency;
    emit criticalFrequencyChanged(m_criticalFrequency);
}

void OscillationDetector::setCriticalAmplitude(float criticalAmplitude)
{
    if (qFuzzyCompare(m_criticalAmplitude, criticalAmplitude)) {
        return;
    }

    m_criticalAmplitude = criticalAmplitude;
    emit criticalAmplitudeChanged(m_criticalAmplitude);
}

void OscillationDetector::pushValue(float timestamp, const arma::fvec &value)
{
    if (m_buffer.n_cols == 0) {
        m_buffer.set_size(bufferSize(), value.n_elem);
    } else if (m_buffer.n_cols != value.n_elem) {
        qWarning("Size mismatch: buffer cols: %d, pushed value has %d elems", (int) m_buffer.n_cols, (int) value.n_elem);
    }

    m_buffer(m_bufferPos, span::all) = value.t();
    m_timestamps[m_bufferPos] = timestamp;
    m_bufferPos = (m_bufferPos + 1) % bufferSize();
}

void OscillationDetector::checkOscillations()
{
    const fvec timestamps = shift(m_timestamps, -m_bufferPos);
    const fmat buffer = shift(m_buffer, -m_bufferPos);

    Q_ASSERT(timestamps.is_sorted());

    const auto dt = mean(diff(timestamps));
    const auto Fs = 1 / dt;
    fmat Y = abs(fft(buffer.each_col() % hann<fvec>(bufferSize()))) * dt;

    // We're only interested in the first (non-mirrored) half of the spectrum
    Y = Y(span(0, bufferSize() / 2 - 1), span::all);
    const fvec F = linspace<fvec>(0, Fs / 2.f, Y.n_rows);

    const auto Acandidates = (Y > m_criticalAmplitude).eval();

    QVector<int> oscillatingChannels;

    for (int i = 0; i < m_buffer.n_cols; ++i) {
        const auto freqCandiditates = F(find(Acandidates(span::all, i))).eval();
        const auto oscillationFrequencies = freqCandiditates(find(freqCandiditates > m_criticalFrequency)).eval();

        if (!oscillationFrequencies.empty()) {
            oscillatingChannels << i;
        }
    }

    emit oscillationsDetected(oscillatingChannels);
}
