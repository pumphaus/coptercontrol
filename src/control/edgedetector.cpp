#include "edgedetector.h"

#include <QTimerEvent>

#include "config.h"

#if !COPTERCONTROL_SIMULATOR
#include "blasterpp/blasterpp.h"
#endif

EdgeDetector::EdgeDetector(QObject *parent)
    : QObject(parent)
{
    m_pollTimer.start(0, Qt::PreciseTimer, this);
}

EdgeDetector::EdgeDetector(const QVector<int> &pins, QObject *parent)
    : QObject(parent)
{
    setPins(pins);
}

QVector<int> EdgeDetector::pins() const
{
    return m_pins;
}

void EdgeDetector::setPins(const QVector<int> &pins)
{
    if (m_pins == pins) {
        return;
    }

    m_pins = pins;

#if !COPTERCONTROL_SIMULATOR
    for (int pin : m_pins) {
        BlasterPP::setGpioMode(pin, BlasterPP::ModeInput);
    }
#endif

    emit pinsChanged(m_pins);
}

void EdgeDetector::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == m_pollTimer.timerId()) {
        checkEdges();
    }
}

void EdgeDetector::checkEdges()
{
#if !COPTERCONTROL_SIMULATOR
    const quint32 curMask = BlasterPP::gpioValues();
    for (int pin : m_pins) {
        const bool last = m_lastMask & (1 << pin);
        const bool cur = curMask & (1 << pin);
        if (cur && !last) {
            emit risingEdge(pin);
        } else if (last && !cur) {
            emit fallingEdge(pin);
        }
    }
    m_lastMask = curMask;
#endif
}
