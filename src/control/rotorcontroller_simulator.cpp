#include "rotorcontroller.h"

#include <QtDebug>

#include <stepcore/motor.h>
#include <stepcore/world.h>

#include "math_util.h"

namespace {
const QString LeftRotorName = "leftRotor";
const QString RightRotorName = "rightRotor";

constexpr double fc2tau(double fc)
{
    return 1.0 / (2 * M_PI * fc);
}

float throttle2Force(float throttle)
{
    // See rotor_force.ipynb
    return 4.53631144 * throttle + 0.28790324;
}

}

RotorController::RotorController(QObject *parent)
    : QObject(parent)
{
    for (auto &filter : m_forceLatency) {
        // Assume that force has a cutoff frequency of 50 Hz
        filter.tau = fc2tau(50);
    }
}

RotorController::~RotorController()
{
    setEnabled(false);
}

arma::fvec4 RotorController::throttle() const
{
    return m_throttle;
}

float RotorController::throttle(RotorController::Rotor rotor) const
{
    return m_throttle[rotor];
}

bool RotorController::isEnabled() const
{
    return m_enabled;
}

void RotorController::setThrottle(const arma::fvec4 &throttle)
{
    updateOutput();
    if (all(m_throttle == throttle)) {
        return;
    }

    m_throttle = throttle;
    emit throttleChanged(m_throttle);
}

void RotorController::setThrottle(RotorController::Rotor rotor, float throttle)
{
    auto curThrottle = this->throttle();
    curThrottle[rotor] = throttle;
    setThrottle(curThrottle);
}

void RotorController::setEnabled(bool enabled)
{
    if (m_enabled == enabled) {
        return;
    }

    m_enabled = enabled;
    updateOutput();
    emit enabledChanged(m_enabled);
}

void RotorController::updateOutput()
{
    if (!item() || !item()->world()) {
        return;
    }

    auto leftMotorIt = std::find_if(item()->world()->forces().begin(),
                                    item()->world()->forces().end(),
                                    [](auto &&force)
    {
        return force->name() == LeftRotorName;
    });
    auto rightMotorIt = std::find_if(item()->world()->forces().begin(),
                                     item()->world()->forces().end(),
                                     [](auto &&force)
    {
        return force->name() == RightRotorName;
    });

    if (leftMotorIt == item()->world()->forces().end()
            || rightMotorIt == item()->world()->forces().end())
    {
        qWarning() << "Failed to find a rotor!";
        return;
    }

    auto leftMotor = static_cast<StepCore::LinearMotor*>(*leftMotorIt);
    auto rightMotor = static_cast<StepCore::LinearMotor*>(*rightMotorIt);

    const auto throttle = arma::clamp(m_throttle, 1e-6f, 1.f).eval();

    leftMotor->setForceNorm(m_forceLatency[0](
                m_enabled ? throttle2Force(throttle[FrontLeft] + throttle[RearLeft])
                          : 0));
    rightMotor->setForceNorm(m_forceLatency[1](
                m_enabled ? throttle2Force(throttle[FrontRight] + throttle[RearRight])
                          : 0));
}
