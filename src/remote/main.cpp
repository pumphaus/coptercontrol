#include <QCoreApplication>
#include <QCborValue>
#include <QDataStream>
#include <QGamepad>
#include <QGamepadManager>
#include <QTimer>
#include <QVariantMap>

#include <QTextStream>

#include <QNetworkDatagram>
#include <QUdpSocket>

#include <QtDebug>

#include <armadillo>

QString number(double num, int prec = 2)
{
    QString s = QString::number(num, 'f', prec);
    return s.rightJustified(7);
}

template<class T>
QString number(const arma::Col<T> &col, int prec = 2)
{
    QString r;
    for (auto &&e : col) {
        r.append(number(e, prec));
    }
    return r;
}

template<class T>
QVariantList toVariantList(const arma::Col<T> &col)
{
    QVariantList list;
    list.reserve(col.size());
    for (auto &&e : col) {
        list.append(QVariant::fromValue(e));
    }
    return list;
}

static constexpr int Port = 45450;
static constexpr int BroadcastPort = 45451;

QHostAddress discoverCopterHost()
{
    qDebug() << "Waiting for host...";

    QUdpSocket socket;
    socket.bind(QHostAddress::AnyIPv4, BroadcastPort);

    QEventLoop loop;

    QHostAddress address;

    QObject::connect(&socket, &QUdpSocket::readyRead, [&]() {
        QNetworkDatagram datagram;
        while (socket.hasPendingDatagrams()) {
            datagram = socket.receiveDatagram();
            address = datagram.senderAddress();
            if (datagram.data() == "COPTERCONTROL") {
                loop.quit();
                return;
            }
        }
    });

    loop.exec();

    qDebug() << "Found host at" << address;

    return address;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QHostAddress address = discoverCopterHost();

    const auto pads = QGamepadManager::instance()->connectedGamepads();
    for (auto pad : pads) {
        qDebug() << "Connected pad" << pad << QGamepadManager::instance()->gamepadName(pad);
    }
    if (pads.isEmpty()) {
        qFatal("No gamepad connected!");
        return 1;
    }

    const int padId = pads[0];

    qDebug() << "Using" << QGamepadManager::instance()->gamepadName(padId);

    QGamepad gamepad(padId);

    QUdpSocket socket;

    QTimer timer;
    timer.setTimerType(Qt::PreciseTimer);
    timer.setInterval(20);

    QTextStream out(stdout);

    bool enabled = false;
    QObject::connect(&gamepad, &QGamepad::buttonAChanged, [&](bool pressed) {
        if (!pressed) {
            enabled = !enabled;
        }
    });

    timer.callOnTimeout([&]() {
        double climbRate = (gamepad.buttonL2() - gamepad.buttonR2());
        arma::vec angularRates {
            gamepad.axisLeftX(),
            gamepad.axisLeftY(),
            gamepad.axisRightX(),
        };
        angularRates *= 90 /*degree / s*/;

        QVariantMap map;
        map["t"] = QDateTime::currentMSecsSinceEpoch();
        map["ena"] = enabled;
        map["a"] = QVariantList { 0., 0., 0. };
        map["ar"] = toVariantList(angularRates);
        map["cr"] = climbRate;

        const auto data = QCborValue::fromVariant(map).toCbor();
        socket.writeDatagram(data, address, Port);

        out << '\r'
            << "Climb rate: " << number(climbRate) << "\t"
            << "Rates: " << number(angularRates) << "\t"
            << "Enabled: " << enabled << "\t"
            << Qt::flush;
    });

    timer.start();

    return a.exec();
}
